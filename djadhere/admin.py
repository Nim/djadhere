from django.contrib import admin
from django.contrib.admin.models import ADDITION, CHANGE, DELETION, LogEntry
from django.contrib.auth.models import User
from django.urls import NoReverseMatch, reverse
from django.utils.html import escape

action_names = {
    ADDITION: "Addition",
    CHANGE: "Change",
    DELETION: "Deletion",
}


class FilterBase(admin.SimpleListFilter):
    def queryset(self, request, queryset):
        if self.value():
            dictionary = {self.parameter_name: self.value()}
            return queryset.filter(**dictionary)
        return None


class ActionFilter(FilterBase):
    title = "action"
    parameter_name = "action_flag"

    def lookups(self, request, model_admin):
        return action_names.items()


class UserFilter(FilterBase):
    """Use this filter to only show current users, who appear in the log."""

    title = "user"
    parameter_name = "user_id"

    def lookups(self, request, model_admin):
        return tuple(
            (u.id, u.username)
            for u in User.objects.filter(
                pk__in=LogEntry.objects.values_list("user_id").distinct(),
            )
        )


class AdminFilter(UserFilter):
    """Use this filter to only show current Superusers."""

    title = "admin"

    def lookups(self, request, model_admin):
        return tuple((u.id, u.username) for u in User.objects.filter(is_superuser=True))


class StaffFilter(UserFilter):
    """Use this filter to only show current Staff members."""

    title = "staff"

    def lookups(self, request, model_admin):
        return tuple((u.id, u.username) for u in User.objects.filter(is_staff=True))


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    date_hierarchy = "action_time"

    readonly_fields = [field.name for field in LogEntry._meta.get_fields()]

    list_filter = [
        UserFilter,
        ActionFilter,
        "content_type",
        # 'user',
    ]

    search_fields = ["object_repr", "change_message"]

    list_display = [
        "action_time",
        "user",
        "content_type",
        "get_object_link",
        "action_flag",
        "action_description",
        "change_message",
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != "POST"

    def has_delete_permission(self, request, obj=None):
        return False

    @admin.display(
        description="object",
        ordering="object_repr",
    )
    def get_object_link(self, obj):
        ct = obj.content_type
        repr_ = escape(obj.object_repr)
        try:
            href = reverse(
                f"admin:{ct.app_label}_{ct.model}_change",
                args=[obj.object_id],
            )
            link = f'<a href="{href}">{repr_}</a>'
        except (NoReverseMatch, AttributeError):
            link = repr_
        return link if obj.action_flag != DELETION else repr_

    def queryset(self, request):
        return super().queryset(request).prefetch_related("content_type")

    @admin.display(
        description="Action",
    )
    def action_description(self, obj):
        return action_names[obj.action_flag]
