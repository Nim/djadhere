from functools import wraps

from django.conf import settings
from django.core.exceptions import PermissionDenied


def api_key_required(view_func):
    def wrapped_view(request, **kwargs):
        key = request.GET.get("key") or request.POST.get("key")
        if key != settings.API_KEY:
            raise PermissionDenied
        return view_func(request, **kwargs)

    return wraps(view_func)(wrapped_view)
