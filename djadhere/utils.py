import json
import logging
import socket
from collections import namedtuple

from django.conf import settings
from django.contrib.admin import SimpleListFilter
from django.core.mail.message import EmailMultiAlternatives
from django.db.models import Q
from django.forms import widgets
from django.utils import timezone
from django.utils.safestring import mark_safe

logger = logging.getLogger(__name__)


class ActiveFilter(SimpleListFilter):
    title = "actif"
    parameter_name = "active"

    def lookups(self, request, model_admin):
        return (
            ("?", "Non renseigné"),
            ("0", "Inactif"),
            ("1", "Actif"),
        )

    def queryset(self, request, queryset):
        if self.value() == "?":
            return queryset.filter(active__isnull=True)
        if self.value() == "0":
            return queryset.filter(active=False)
        if self.value() == "1":
            return queryset.filter(active=True)
        return None


# Ce widget permet d`afficher un champ de formulaire sous forme de texte
class StringWidget(widgets.Input):
    def render(self, name, value, attrs=None):
        # Create a hidden field first
        hidden_field = widgets.HiddenInput(attrs)
        return mark_safe(f"{value} {hidden_field.render(value, attrs)}")


def get_active_filter(prefix=""):
    """
    Cette fonction permet d`obtenir un filtre s`appliquant aux paiements
    et aux allocations de ressources
    """
    if prefix and not prefix.endswith("__"):
        prefix += "__"
    now = timezone.now()
    # Début antérieur et fin non spécifié ou postérieur
    return Q(**{prefix + "start__lte": now}) & (
        Q(**{prefix + "end__isnull": True}) | Q(**{prefix + "end__gte": now})
    )


def is_overlapping(instance, queryset):
    """
    Cette fonction vérifie que l`object « instance » ne chevauche pas temporellement un
    objet présent dans « queryset »
    Le model associé doit posséder deux attributs « start » et « end ».
    L`attribut « start » doit être obligatoire.
    """
    # Le champ « start » ne doit pas être None pour les comparaisons qui suivent
    if not instance.start:
        return False  # Une erreur sera levé au niveau de ce field spécifiquement

    # S`il s`agit d`une modification et non d`un ajout, il faut ignorer l`objet déjà
    # présent en db
    if instance.pk:
        queryset = queryset.exclude(pk=instance.pk)

    for existing in queryset.all():
        if instance.end and existing.end:
            # Les deux périodes sont terminées
            latest_start = max(instance.start, existing.start)
            earliest_end = min(instance.end, existing.end)
            if earliest_end > latest_start:
                return True
        elif existing.end:
            assert not instance.end
            # La période existante est terminée et doit donc se terminer avant la
            # modifiée
            if existing.end > instance.start:
                return True
        elif instance.end:
            assert not existing.end
            # La période existante n`est terminée, la modifiée doit se terminer avant
            if instance.end > existing.start:
                return True
        else:
            assert not instance.end and not existing.end
            # Aucune des périodes n`est terminées
            return True
    return None


def send_notification(subject, message, recipients, **kwargs):
    mail = EmailMultiAlternatives(
        f"{settings.EMAIL_SUBJECT_PREFIX}{subject}",
        message,
        settings.SERVER_EMAIL,
        recipients,
        **kwargs,
    )
    mail.send()


def send_web_notification(subject, message):
    if settings.WEBHOOK:
        import requests

        try:
            requests.post(
                settings.WEBHOOK_URL,
                data=json.dumps(
                    {
                        "text": f"{subject}\n{message}",
                        "key": settings.WEBHOOK_KEY,
                    },
                ),
            )
        except requests.exceptions.ConnectionError as e:
            err = f"can't send web notification: {e}"
            logger.error(err)


def from_livestatus(get, query=None, columns=None):
    if query is None:
        query = []
    if columns is None:
        columns = []
    query = ["GET %s" % get, *query]
    if columns:
        query += ["Columns: " + " ".join(columns)]
    query += ["OutputFormat: json"]
    query = "".join(s + "\n" for s in query)
    if columns:
        line = namedtuple(get.capitalize(), columns)
    else:
        line = None
    lines = []
    try:
        with socket.create_connection(("nucnagios.tetaneutral.net", "8622")) as sock:
            sock.send(query.encode("utf-8"))
            sock.shutdown(socket.SHUT_WR)
            infile = sock.makefile(encoding="utf-8")
            data = json.load(infile)
    except ConnectionResetError:
        pass
    else:
        for entry in data:
            if not line:
                line = namedtuple(get.capitalize(), entry)
                continue
            lines.append(line(*entry))
    return lines
