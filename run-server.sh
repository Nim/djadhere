#!/bin/bash


BASEDIR="$(dirname $0)"

. "$BASEDIR"/venv/bin/activate

"$BASEDIR"/manage.py runserver --settings=djadhere.$(hostname)_settings $(hostname):8000
