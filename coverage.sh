#!/bin/bash -eux


BASEDIR="$(dirname "$0")"

if test -z "$VIRTUAL_ENV"
then
    . "$BASEDIR"/venv/bin/activate
fi

if ! which coverage > /dev/null 2>&1
then
  pip install coverage
fi

set -x

coverage run --branch --source=djadhere,accounts,adhesions,banking,services,stocking,todo --omit=*/migrations/*.py "$BASEDIR"/manage.py test
coverage html
coverage report
