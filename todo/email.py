from django.conf import settings
from django.core import mail
from django.template.loader import render_to_string


def send_task_notification(task, user, created):
    if not created:
        return
    subject = settings.EMAIL_SUBJECT_PREFIX + task.title
    context = {
        "task": task,
        "body": task.note,
        "domain": settings.TODO_DOMAIN,
        "user": user,
    }
    txt_body = render_to_string("todo/email/newtask.txt", context)
    to_addresses = task.watchers.exclude(email="").values_list("email", flat=True)
    send_task_email(task, user, subject, txt_body, to_addresses)


def send_comment_notification(comment, user):
    task = comment.task
    subject = "Re: " + settings.TODO_SUBJECT_PREFIX + task.title
    context = {
        "task": task,
        "body": comment.body,
        "domain": settings.TODO_DOMAIN,
        "user": user,
    }
    txt_body = render_to_string("todo/email/newcomment.txt", context)
    to_addresses = task.watchers.exclude(email="").values_list("email", flat=True)
    send_task_email(task, user, subject, txt_body, to_addresses, comment=comment)


def send_task_email(
    task,
    user,
    subject,
    body,
    to_addresses,
    alternatives=None,
    comment=None,
):
    if alternatives is None:
        alternatives = []
    from_address = "{} <{}>".format(
        user.profile,
        getattr(settings, "TODO_FROM_EMAIL", settings.DEFAULT_FROM_EMAIL),
    )
    thread_id = f"<todo/{task.pk}@{settings.TODO_DOMAIN}>"
    if comment:
        headers = {
            "Message-ID": f"<todo/{task.pk}/{comment.pk}@{settings.TODO_DOMAIN}>",
            "References": thread_id,
            "In-reply-to": thread_id,
        }
    else:
        headers = {
            "Message-ID": thread_id,
        }

    with mail.get_connection() as connection:
        message = mail.EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=from_address,
            to=to_addresses,
            connection=connection,
            headers=headers,
        )
        for alternative_content, alternative_mime in alternatives:
            message.attach_alternative(alternative_content, alternative_mime)
        message.send()
