from django.urls import path

from . import views

app_name = "todo"


urlpatterns = [
    path("", views.tasklist_list, name="list-tasklists"),
    path("<str:tasklist_slug>/", views.tasklist_detail, name="show-tasklist"),
    path(
        "<str:tasklist_slug>/completed/",
        views.tasklist_detail,
        {"completed": True},
        name="show-tasklist-completed",
    ),
    path(
        "<str:tasklist_slug>/reorder/",
        views.tasklist_reorder,
        name="reorder-tasklist",
    ),
    path(
        "<str:tasklist_slug>/subscribe/",
        views.tasklist_subscribe,
        {"subscribe": True},
        name="subscribe-tasklist",
    ),
    path(
        "<str:tasklist_slug>/unsubscribe/",
        views.tasklist_subscribe,
        {"subscribe": False},
        name="unsubscribe-tasklist",
    ),
    path("<str:tasklist_slug>/add/", views.task_form, name="add-task"),
    path("<str:tasklist_slug>/<int:task_id>/", views.task_detail, name="show-task"),
    path(
        "<str:tasklist_slug>/<int:task_id>/toggle-done/",
        views.task_toggle_done,
        name="toggle-task-done",
    ),
    path(
        "<str:tasklist_slug>/<int:task_id>/subscribe/",
        views.task_subscribe,
        {"subscribe": True},
        name="subscribe-task",
    ),
    path(
        "<str:tasklist_slug>/<int:task_id>/unsubscribe/",
        views.task_subscribe,
        {"subscribe": False},
        name="unsubscribe-task",
    ),
    path("<str:tasklist_slug>/<int:task_id>/edit/", views.task_form, name="edit-task"),
    path(
        "<str:tasklist_slug>/<int:task_id>/delete/",
        views.task_delete,
        name="delete-task",
    ),
]
