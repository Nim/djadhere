import datetime
import textwrap

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models import Q
from django.utils import timezone


class TaskList(models.Model):
    name = models.CharField(max_length=60, verbose_name="nom")
    slug = models.SlugField(null=False, blank=False, unique=True)
    groups = models.ManyToManyField(Group, verbose_name="groupe", blank=True)
    subscribers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="subscribed_tasklists",
    )

    @property
    def completed_task_set(self):
        return self.task_set.filter(completed_date__isnull=False)

    @property
    def uncompleted_task_set(self):
        return self.task_set.filter(completed_date__isnull=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name = "liste de tâches"
        verbose_name_plural = "listes de tâches"


class Task(models.Model):
    title = models.CharField(max_length=140, verbose_name="titre")
    task_list = models.ForeignKey(TaskList, on_delete=models.PROTECT)
    created_date = models.DateField(default=timezone.now, blank=True, null=True)
    due_date = models.DateField(blank=True, null=True, verbose_name="due pour le")
    completed_date = models.DateField(blank=True, null=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name="created_task_set",
        verbose_name="créée par",
    )
    assigned_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="assigned_task_set",
        verbose_name="assignée à",
    )
    note = models.TextField(blank=True)
    priority = models.PositiveIntegerField(blank=True, null=True)
    subscribers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="subscribed_tasks",
    )
    unsubscribers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="unsubscribed_tasks",
    )

    def subscribed(self, user):
        if user in self.task_list.subscribers.all():
            return user not in self.unsubscribers.all()
        return user in self.subscribers.all()

    @property
    def watchers(self):
        return (
            get_user_model()
            .objects.filter(
                (Q(subscribed_tasklists=self.task_list) & ~Q(unsubscribed_tasks=self))
                | (~Q(subscribed_tasklists=self.task_list) & Q(subscribed_tasks=self)),
            )
            .order_by("pk")
            .distinct("pk")
        )

    def overdue_status(self):
        "Returns whether the Tasks's due date has passed or not."
        if self.due_date and datetime.date.today() > self.due_date:
            return True
        return None

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["priority", "created_date"]
        verbose_name = "tâche"
        verbose_name_plural = "tâches"


class TaskComment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="comments")
    date = models.DateTimeField(default=timezone.now)
    body = models.TextField(blank=True)

    @property
    def snippet(self):
        body_snippet = textwrap.shorten(self.body, width=35, placeholder="...")
        return f"{self.author!s} - {body_snippet}..."

    def __str__(self):
        return self.snippet
