from django import forms

from adhesions.models import User

from .models import Task, TaskComment


def get_label_from_instance(obj):
    return "ADT%d %s" % (
        obj.adhesion.id,
        str(obj.profile),
    )


class TaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        tasklist = kwargs.pop("tasklist")
        super().__init__(*args, **kwargs)
        if tasklist.groups.exists():
            members = User.objects.filter(groups__in=tasklist.groups.all())
        else:
            members = User.objects.all()
        members = members.order_by("adhesion__id")
        members = members.select_related("adhesion")
        self.fields["assigned_to"].queryset = members
        self.fields["assigned_to"].label_from_instance = get_label_from_instance

    class Meta:
        model = Task
        fields = (
            "title",
            "note",
            "due_date",
            "assigned_to",
        )
        widgets = {
            "due_date": forms.DateInput(attrs={"type": "date"}),
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = TaskComment
        fields = ("body",)
