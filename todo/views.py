from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.http import require_POST

from .decorators import allowed_tasklist_required
from .email import send_comment_notification, send_task_notification
from .forms import CommentForm, TaskForm
from .models import Task, TaskList


@login_required
def tasklist_list(request):
    lists = TaskList.objects.all()
    if not request.user.is_superuser:
        lists = lists.filter(groups__in=request.user.groups.all())
    lists = lists.order_by("name")
    lists = lists.annotate(
        own_uncompleted_task_count=Count(
            "task",
            filter=Q(task__completed_date__isnull=True, task__assigned_to=request.user),
        ),
    )
    return render(
        request,
        "todo/tasklist_list.html",
        {
            "lists": lists,
            "task_count": Task.objects.filter(
                completed_date__isnull=True,
                task_list__in=lists,
            ).count(),
            "list_count": lists.count(),
        },
    )


@allowed_tasklist_required
def tasklist_detail(request, tasklist, completed=False):
    task_list = tasklist.task_set.filter(completed_date__isnull=not completed)
    return render(
        request,
        "todo/tasklist_detail.html",
        {
            "tasklist": tasklist,
            "task_list": task_list,
            "completed": completed,
        },
    )


@allowed_tasklist_required
def tasklist_reorder(request, tasklist):
    newtasklist = request.POST.getlist("tasktable[]")
    if newtasklist:
        # Re-prioritize each task in list
        i = 1
        for pk in newtasklist:
            try:
                task = Task.objects.get(task_list=tasklist, pk=pk)
                task.priority = i
                task.save()
                i += 1
            except Task.DoesNotExist:
                # Can occur if task is deleted behind the scenes during re-ordering.
                # Not easy to remove it from the UI without page refresh, but prevent
                # crash.
                pass
    # All views must return an httpresponse of some kind ... without this we get
    # error 500s in the log even though things look peachy in the browser.
    return HttpResponse(status=201)


@allowed_tasklist_required
def tasklist_subscribe(request, tasklist, subscribe):
    if subscribe:
        tasklist.subscribers.add(request.user)
    else:
        tasklist.subscribers.remove(request.user)
    return redirect(
        reverse("todo:show-tasklist", kwargs={"tasklist_slug": tasklist.slug}),
    )


@allowed_tasklist_required
def task_form(request, tasklist, task_id=None):
    if task_id:
        task = get_object_or_404(Task, task_list=tasklist, pk=task_id)
        redirect_url = reverse(
            "todo:show-task",
            kwargs={"tasklist_slug": tasklist.slug, "task_id": task.pk},
        )
    else:
        task = None
        redirect_url = reverse(
            "todo:show-tasklist",
            kwargs={"tasklist_slug": tasklist.slug},
        )
    form = TaskForm(request.POST or None, tasklist=tasklist, instance=task)
    if request.method == "POST" and form.is_valid():
        if task:
            task = form.save()
            task.subscribers.add(request.user)
            if task.assigned_to and task.assigned_to not in task.unsubscribers.all():
                task.subscribers.add(task.assigned_to)
            send_task_notification(task, request.user, created=False)
            messages.success(request, "Tâche mise à jour avec succès.")
        else:
            task = form.save(commit=False)
            task.task_list = tasklist
            task.created_by = request.user
            task.save()
            task.subscribers.add(request.user)
            if task.assigned_to and task.assigned_to not in task.unsubscribers.all():
                task.subscribers.add(task.assigned_to)
            send_task_notification(task, request.user, created=True)
            messages.success(request, "Tâche créée avec succès.")
        return redirect(redirect_url)
    return render(
        request,
        "todo/task_form.html",
        {
            "tasklist": tasklist,
            "task": task,
            "form": form,
            "cancel_url": redirect_url,
        },
    )


@allowed_tasklist_required
def task_detail(request, tasklist, task_id):
    task = get_object_or_404(Task, task_list=tasklist, pk=task_id)
    form = CommentForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        comment = form.save(commit=False)
        comment.task = task
        comment.author = request.user
        comment.save()
        task.subscribers.add(request.user)
        send_comment_notification(comment, request.user)
        messages.success(request, "Commentaire ajouté avec succès.")
        return redirect(
            reverse(
                "todo:show-task",
                kwargs={"tasklist_slug": tasklist.slug, "task_id": task_id},
            ),
        )
    return render(
        request,
        "todo/task_detail.html",
        {
            "tasklist": tasklist,
            "task": task,
            "form": form,
            "subscribed": task.subscribed(request.user),
        },
    )


@allowed_tasklist_required
def task_toggle_done(request, tasklist, task_id):
    task = get_object_or_404(Task, task_list=tasklist, pk=task_id)
    if task.completed_date:
        task.completed_date = None
        messages.success(request, f"La tâche « {task.title} » a été marquée en cours.")
    else:
        task.completed_date = timezone.now()
        messages.success(request, f"La tâche « {task.title} » a été marquée complétée.")
    task.save()
    return redirect(
        reverse(
            "todo:show-task",
            kwargs={"tasklist_slug": tasklist.slug, "task_id": task_id},
        ),
    )


@allowed_tasklist_required
def task_subscribe(request, tasklist, task_id, subscribe):
    task = get_object_or_404(Task, task_list=tasklist, pk=task_id)
    if subscribe:
        task.unsubscribers.remove(request.user)
        # if request.user not in tasklist.subscribers.all():
        task.subscribers.add(request.user)
    else:
        task.subscribers.remove(request.user)
        # if request.user in tasklist.subscribers.all():
        task.unsubscribers.add(request.user)
    return redirect(
        reverse(
            "todo:show-task",
            kwargs={"tasklist_slug": tasklist.slug, "task_id": task_id},
        ),
    )


# TODO: are you sure?
@require_POST
@allowed_tasklist_required
def task_delete(request, tasklist, task_id):
    task = get_object_or_404(Task, task_list=tasklist, pk=task_id)
    task.delete()
    messages.success(request, f"La tâche « {task.title} » a été supprimée.")
    return redirect(
        reverse("todo:show-tasklist", kwargs={"tasklist_slug": tasklist.slug}),
    )
