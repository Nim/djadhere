from datetime import date

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test import TestCase
from django.urls import reverse

from accounts.models import Profile
from adhesions.models import Adhesion

from .models import Task, TaskComment, TaskList


class TestTodo(TestCase):
    def setUp(self):
        User = get_user_model()  # noqa: N806
        u1 = User.objects.create_user(username="u1", password="u1")
        u2 = User.objects.create_user(username="u2", password="u2")
        u3 = User.objects.create_user(username="u3", password="u3")
        u4 = User.objects.create_user(username="u4", password="u4")
        u5 = User.objects.create_user(username="u5", password="u5")
        u6 = User.objects.create_user(username="u6", password="u6")
        u7 = User.objects.create_user(username="u7", password="u7")
        User.objects.create_user(username="u8", password="u8")
        g1 = Group.objects.create(name="g1")
        u1.groups.add(g1)
        u1.save()
        # TODO: autoslug…
        tasklist = TaskList.objects.create(name="todo", slug="todo")
        tasklist.subscribers.add(u1)
        tasklist.subscribers.add(u2)
        tasklist.subscribers.add(u3)
        tasklist.subscribers.add(u4)
        tasklist.groups.add(g1)
        tasklist.save()
        task = Task.objects.create(title="Tâche 1", task_list=tasklist, created_by=u1)
        task.subscribers.add(u1)
        task.subscribers.add(u2)
        task.subscribers.add(u5)
        task.subscribers.add(u6)
        task.unsubscribers.add(u1)
        task.unsubscribers.add(u3)
        task.unsubscribers.add(u5)
        task.unsubscribers.add(u7)
        task.save()
        admin = User.objects.create_user(
            "admin",
            email="admin@example.net",
            password="admin",
            is_staff=True,
            is_superuser=True,
        )
        tasklist2 = TaskList.objects.create(name="tasklist2", slug="tasklist2")
        Task.objects.create(title="Tâche 2", task_list=tasklist2, created_by=admin)

    def test_watchers(self):
        User = get_user_model()  # noqa: N806
        task = Task.objects.first()
        self.assertIn(User.objects.get(username="u2"), task.watchers)
        self.assertIn(User.objects.get(username="u4"), task.watchers)
        self.assertIn(User.objects.get(username="u5"), task.watchers)
        self.assertIn(User.objects.get(username="u6"), task.watchers)
        self.assertEqual(task.watchers.count(), 4)

    def test_views(self):
        self.client.login(username="u1", password="u1")
        response = self.client.get(reverse("todo:list-tasklists"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            "1 tâche en cours dans 1 liste</p>",
            response.content.decode(),
        )

        self.client.login(username="u2", password="u2")
        response = self.client.get(reverse("todo:list-tasklists"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            "0 tâches en cours dans 0 listes</p>",
            response.content.decode(),
        )

        self.client.login(username="admin", password="admin")
        response = self.client.get(reverse("todo:list-tasklists"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            "2 tâches en cours dans 2 listes</p>",
            response.content.decode(),
        )

        response = self.client.get(reverse("todo:show-tasklist", args=["todo"]))
        self.assertEqual(response.status_code, 200)

        # here, it looks like we need additionnal Adhesion / Profile. I don't know why.
        # I guess they should be created automatically, but aren't.

        for user in get_user_model().objects.all():
            Profile.objects.get_or_create(user=user)
            Adhesion.objects.get_or_create(user=user)

        response = self.client.get(reverse("todo:add-task", args=["todo"]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(Task.objects.count(), 2)
        response = self.client.post(
            reverse("todo:add-task", args=["todo"]),
            {
                "title": "do something",
                "note": "bene",
                "due_date": date.today(),
                "assigned_to": get_user_model().objects.first().pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.count(), 3)

        task = Task.objects.get(title="do something")
        self.assertEqual(task.note, "bene")
        response = self.client.post(
            reverse("todo:edit-task", args=["todo", task.pk]),
            {
                "title": "do something",
                "note": "benebene",
                "due_date": date.today(),
                "assigned_to": get_user_model().objects.first().pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.count(), 3)
        task = Task.objects.get(title="do something")
        self.assertEqual(task.note, "benebene")

        response = self.client.get(reverse("todo:show-task", args=["todo", task.pk]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(TaskComment.objects.count(), 0)
        response = self.client.post(
            reverse("todo:show-task", args=["todo", task.pk]),
            {"body": "hello"},
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(TaskComment.objects.count(), 1)
