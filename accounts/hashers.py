import base64
import hashlib

from django.contrib.auth.hashers import PBKDF2PasswordHasher
from django.utils.crypto import get_random_string, pbkdf2


class PBKDF2DK256PasswordHasher(PBKDF2PasswordHasher):
    algorithm = "pbkdf2_sha256_dk256"
    iterations = 2048
    digest = hashlib.sha256
    dklen = 256

    def salt(self):
        return get_random_string(64)

    def encode(self, password, salt, iterations=None):
        assert password is not None
        assert salt and "$" not in salt
        iterations = iterations or self.iterations
        hash = pbkdf2(password, salt, iterations, digest=self.digest, dklen=self.dklen)
        hash = base64.b64encode(hash).decode("ascii").strip()
        return "%s$%d$%s$%s" % (self.algorithm, iterations, salt, hash)
