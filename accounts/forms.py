from django.contrib.auth.forms import PasswordResetForm as AuthPasswordResetForm
from django.forms import ModelForm, ValidationError

from adhesions.models import User

from .models import Profile


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ("email",)


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            "common_name",
            "phone_number",
            "address",
            "ssh_keys",
        )


class PasswordResetForm(AuthPasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data["email"]
        users = User.objects.filter(email__iexact=email)
        if not users.exists():
            msg = "Aucun utilisateur connu avec cette adresse e-mail."
            raise ValidationError(msg)
        return email
