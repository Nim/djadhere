from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.utils.crypto import get_random_string

from adhesions.models import User

from .models import Profile


@receiver(pre_save, sender=User, dispatch_uid="set_unusable_password")
def set_unusable_password(sender, instance, **kwargs):
    if not instance.password:
        instance.set_password(get_random_string(length=32))


@receiver(pre_save, sender=User, dispatch_uid="set_default_username")
def set_default_username(sender, instance, **kwargs):
    if not instance.username:
        instance.username = slugify(instance.get_full_name())


@receiver(post_save, sender=User, dispatch_uid="create_profile")
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
