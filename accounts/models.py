from django.db import models

from adhesions.models import Adhesion, User


class Profile(models.Model):
    user = models.OneToOneField(
        User,
        related_name="profile",
        verbose_name="Utilisateur",
        on_delete=models.CASCADE,
    )
    common_name = models.CharField(
        max_length=32,
        blank=True,
        default="",
        verbose_name="Nom d`usage",
    )
    phone_number = models.CharField(
        max_length=16,
        blank=True,
        default="",
        verbose_name="Numéro de téléphone",
    )
    address = models.TextField(blank=True, default="", verbose_name="Adresse")
    ssh_keys = models.TextField(
        blank=True,
        default="",
        verbose_name="Clefs SSH",
        help_text="une clef ou url vers des clefs par ligne",
    )
    notes = models.TextField(blank=True, default="")

    class Meta:
        verbose_name = "profil"

    @property
    def adhesions(
        self,
    ):  # user and corporations (for which the user is a member) adhesions
        return Adhesion.objects.filter(
            models.Q(user__pk=self.user.pk)
            | models.Q(corporation__members__profile__pk=self.pk),
        )

    def __str__(self):
        return self.common_name or self.user.get_full_name() or self.user.username
