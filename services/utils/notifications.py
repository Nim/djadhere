from django.conf import settings
from django.urls import reverse

from djadhere.utils import send_notification, send_web_notification


# À simplifier : seul la route peut changer maintenant
def notify_allocation(request, new_alloc, old_alloc=None):
    fields = ["resource", "service", "route", "start", "end"]

    benevole = f"{request.user.username} <{request.user.email}>"
    message = "Bénévole : " + benevole
    message += "\n\nAllocation :\n```"

    diff = False
    for field in fields:
        new_attr = getattr(new_alloc, field)
        if new_attr == "" or new_attr is None:
            new_attr = "-"
        if old_alloc:
            old_attr = getattr(old_alloc, field)
            if old_attr == "" or old_attr is None:
                old_attr = "-"
        if old_alloc and old_attr != new_attr:
            message += "\n-%12s: %s\n+%12s: %s" % (field, old_attr, field, new_attr)
            diff = True
        else:
            message += "\n %12s: %s" % (field, new_attr)

    url = "https" if request.is_secure() else "http"
    url += "://" + request.get_host()
    url += reverse("admin:services_ipresource_change", args=(new_alloc.resource.pk,))
    message += "\n```\nVoir : " + url

    if old_alloc and diff:
        sujet = "Modification d`allocation"
    elif not old_alloc:
        if new_alloc.end:
            sujet = "Fin d`allocation"
        else:
            sujet = "Nouvelle allocation"
    else:
        sujet = None

    if sujet:
        sujet += f" {new_alloc.resource} sur {new_alloc.route}"
        send_notification(sujet, message, settings.ALLOCATIONS_EMAILS, cc=[benevole])
        send_web_notification(sujet, message)
