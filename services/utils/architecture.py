from textwrap import indent

from django.db import models
from django.db.models.functions import Concat

from djadhere.utils import get_active_filter
from services.models import IPPrefix, IPResource, Service, ServiceAllocation, Switch


def export_switch():
    output = []
    for switch in Switch.objects.prefetch_related(
        "ports",
        "ports__service",
        "ports__service__service_type",
        "ports__service__adhesion",
        "ports__service__adhesion__user",
        "ports__service__adhesion__corporation",
    ).all():
        sw_output = "=== Switch %s ===\n" % switch.name
        sw_output += indent(switch.notes, "# ") + "\n"
        ports_output = []
        for port in switch.ports.all():
            line = "Port %d" % port.port
            if port.service:
                line += " - ({} {}) {}".format(
                    port.service.adhesion,
                    port.service.adhesion.adherent,
                    port.service,
                )
            if port.notes:
                line += " - %s" % port.notes
            ports_output += [line]
        sw_output += "\n".join(ports_output)
        output += [sw_output]
    return "\n\n".join(output)


def export_ip():
    output = []
    for prefix in (
        IPPrefix.objects.filter(ipresource__category=IPResource.CATEGORY_PUBLIC)
        .order_by("prefix")
        .distinct()
    ):
        prefix_output = ["=== %s ===" % prefix]
        active_ips = prefix.ipresource_set.filter(in_use=True)
        if not active_ips.exists():
            prefix_output += ["empty"]
            output += ["\n".join(prefix_output)]
            continue
        if active_ips.count() < 20:
            ip_set = active_ips
        else:
            ip_set = prefix.ipresource_set
        ip_set = ip_set.annotate(
            allocation=models.Subquery(
                ServiceAllocation.objects.filter(
                    models.Q(resource=models.OuterRef("pk")) & get_active_filter(),
                ).values("pk")[:1],
            ),
        )
        ip_set = ip_set.annotate(
            service=models.Subquery(
                Service.objects.filter(
                    models.Q(allocation__resource=models.OuterRef("pk"))
                    & get_active_filter("allocation"),
                )
                .annotate(
                    adherent=models.Case(
                        models.When(
                            adhesion__user__isnull=False,
                            then=Concat(
                                "adhesion__user__first_name",
                                models.Value(" "),
                                "adhesion__user__last_name",
                            ),
                        ),
                        models.When(
                            adhesion__corporation__isnull=False,
                            then="adhesion__corporation__social_reason",
                        ),
                    ),
                )
                .annotate(
                    fullname=Concat(
                        models.Value("ADT"),
                        "adhesion_id",
                        models.Value(" "),
                        "adherent",
                        models.Value(" #"),
                        "pk",
                        models.Value(" "),
                        "service_type__name",
                        models.Value(" "),
                        "label",
                        output_field=models.CharField(),
                    ),
                )
                .values("fullname")[:1],
                output_field=models.CharField(),
            ),
        )
        ip_set = ip_set.annotate(
            status=models.Case(
                models.When(reserved=True, then=models.Value("réservé")),
                models.When(allocation__isnull=True, then=models.Value("disponible")),
                default="service",
                output_field=models.CharField(),
            ),
        )
        for ip in ip_set.all():
            line = str(ip)
            line += " - " + ip.status
            prefix_output += [line]
        output += ["\n".join(prefix_output)]
    return "\n\n".join(output)
