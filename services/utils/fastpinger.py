import logging
import re
from ipaddress import AddressValueError, IPv4Address

from django.utils import timezone

from services.models import IPResource, IPResourceState, IPResourceStateCheck

logger = logging.getLogger(__name__)


def fastpinger_update(f):
    now = timezone.now()

    regex = re.compile(
        r"^(?P<ip>[0-9.]+)[ ]+:"
        r" (?P<p1>([0-9]+.[0-9]+|-))"
        r" (?P<p2>([0-9]+.[0-9]+|-))"
        r" (?P<p3>([0-9]+.[0-9]+|-))"
        r" (?P<p4>([0-9]+.[0-9]+|-))"
        r" (?P<p5>([0-9]+.[0-9]+|-))$",
    )

    states = {}
    for line in f:
        try:
            line = line.decode("utf-8")
        except UnicodeDecodeError:
            continue
        g = regex.match(line)
        if not g:
            continue
        try:
            ip = IPv4Address(g.group("ip"))
        except AddressValueError:
            continue
        p = [g.group("p%d" % i) for i in [1, 2, 3, 4, 5]]
        if all(p == "-" for p in p):
            states[ip.compressed] = IPResourceState.STATE_DOWN
        else:
            if ip.compressed not in states:  # do not override a down status
                states[ip.compressed] = IPResourceState.STATE_UP

    down_to_up, up_to_down = 0, 0

    # the special mobile check is a check updated at each run to target present moment
    mobile_check = IPResourceStateCheck.objects.order_by("pk").first()
    if mobile_check:
        mobile_check.date = now
        mobile_check.save()
    else:
        mobile_check = IPResourceStateCheck.objects.create(date=now)
    current_check = IPResourceStateCheck.objects.create(
        date=mobile_check.date,
    )  # to use as begin date for new states
    previous_check = (
        IPResourceStateCheck.objects.exclude(date__gte=mobile_check.date)
        .order_by("date")
        .last()
    )  # to use as end date for leaved states

    for resource in IPResource.objects.select_related(
        "last_state",
        "last_state_up",
        "last_state__end",
    ):
        if (
            resource.ip not in states
        ):  # fastpinger results do not contains any information for this ip -> UNKNOWN
            if (
                resource.last_state and resource.last_state.end == mobile_check
            ):  # ip not already in unknown state
                assert previous_check
                resource.last_state.end = previous_check
                resource.last_state.save()
        else:
            if resource.last_state:
                if resource.last_state.state != states[resource.ip]:  # state changed!
                    if (
                        resource.last_state.end == mobile_check
                    ):  # if not previously in unknown state
                        assert previous_check
                        resource.last_state.end = previous_check
                    resource.last_state.save()
                    if (
                        resource.last_state.state == IPResourceState.STATE_UP
                    ):  # UP -> DOWN
                        # resource.last_state_up = resource.last_state
                        assert resource.last_state_up == resource.last_state
                        up_to_down += 1
                    resource.last_state = IPResourceState.objects.create(
                        ip=resource,
                        state=states[resource.ip],
                        begin=current_check,
                        end=mobile_check,
                    )
                    if (
                        resource.last_state.state == IPResourceState.STATE_UP
                    ):  # DOWN -> UP
                        resource.last_state_up = resource.last_state
                        down_to_up += 1
                    resource.save()
                elif (
                    resource.last_state.end != mobile_check
                ):  # previously unknown, just make state.end follow mobile_check again
                    resource.last_state.end = mobile_check
                    resource.last_state.save()
                else:  # not changed and not previously unknown
                    pass
            else:  # first state for this IP (likely first run)
                resource.last_state = IPResourceState.objects.create(
                    ip=resource,
                    state=states[resource.ip],
                    begin=current_check,
                    end=mobile_check,
                )
                if resource.last_state.state == IPResourceState.STATE_UP:
                    resource.last_state_up = resource.last_state
                resource.save()

    down_count = IPResource.objects.filter(
        last_state__state=IPResourceState.STATE_DOWN,
    ).count()
    up_count = IPResource.objects.filter(
        last_state__state=IPResourceState.STATE_UP,
    ).count()

    return "UP: {} (-{}+{}), DOWN: {} (-{}+{})".format(
        up_count,
        up_to_down,
        down_to_up,
        down_count,
        down_to_up,
        up_to_down,
    )
