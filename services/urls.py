from django.urls import path

from . import views

urlpatterns = [
    path("status/", views.Status.as_view(), name="status"),
    path("services/<int:pk>/", views.ServiceDetail.as_view(), name="service-detail"),
    path("api/fastpinger/", views.fastpinger, name="fastpinger"),
    path("api/routes/", views.routes, name="all-routes"),
    path("api/routes/<slug:route>/", views.routes, name="routes"),
    path("api/architecture/switch/", views.architecture_switch, name="archi-switch"),
    path("api/architecture/ip/", views.architecture_ip, name="archi-ip"),
    path("api/wireguard/", views.wireguard, name="wireguard"),
    path("api/vm/<int:pk>", views.vm, name="vm"),
    path("api/vlans", views.vlans, name="vlans"),
]
