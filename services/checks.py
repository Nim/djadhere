from django.conf import settings
from django.core.checks import Error, register


@register()
def check_settings(app_configs, **kwargs):
    errors = []
    if not hasattr(settings, "ALLOCATIONS_EMAILS"):
        errors.append(Error("Missing settings variable ALLOCATIONS_EMAILS."))
    return errors
