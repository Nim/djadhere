from django.apps import AppConfig


class ServicesConfig(AppConfig):
    name = "services"
    verbose_name = "Services"

    def ready(self):
        import services.checks
        import services.signals  # noqa
