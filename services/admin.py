from ipaddress import IPv4Address

from django.contrib import admin
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import ExpressionWrapper, Q
from django.forms import BaseInlineFormSet, ModelForm
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils import timezone
from django.utils.html import format_html

from adhesions.models import Adhesion
from djadhere.utils import get_active_filter

from .forms import StopAllocationForm
from .models import (
    IPPrefix,
    IPResource,
    IPResourceState,
    Port,
    Route,
    Service,
    ServiceAllocation,
    ServiceType,
    Switch,
    Tunnel,
)
from .utils.ip_conversion import ipv4_to_ipv6
from .utils.notifications import notify_allocation

# ## Filters


class RouteTypeFilter(admin.SimpleListFilter):
    title = "type"
    parameter_name = "type"

    def lookups(self, request, model_admin):
        return (
            ("tun", "Tunnel"),
            ("vlan", "VLAN"),
        )

    def queryset(self, request, queryset):
        if self.value() in ("tun", "vlan"):
            return queryset.filter(name__istartswith=self.value())
        return None


class InUseFilter(admin.SimpleListFilter):
    title = "disponibilité"
    parameter_name = "available"

    def lookups(self, request, model_admin):
        return (
            (1, "Disponible"),
            (0, "Non disponible"),
        )

    def queryset(self, request, queryset):
        available_filter = self.get_available_filter()
        if self.value() == "0":  # non disponible
            return queryset.exclude(available_filter)
        if self.value() == "1":  # disponible
            return queryset.filter(available_filter)
        return None


class RouteInUseFilter(InUseFilter):
    def get_available_filter(self):
        return ~Q(
            pk__in=ServiceAllocation.objects.order_by("route")
            .values_list("route", flat=True)
            .distinct(),
        )


class ResourceInUseFilter(InUseFilter):
    def get_available_filter(self):
        return Q(reserved=False, in_use=False)


class ResourcePingFilter(admin.SimpleListFilter):
    title = "ping"
    parameter_name = "ping"

    def lookups(self, request, model_admin):
        return (
            ("up", "UP"),
            ("down", "DOWN"),
            ("down-since", "DOWN depuis…"),
            ("never-up", "Jamais vu UP"),
        )

    def queryset(self, request, queryset):
        if self.value() == "up":
            return queryset.filter(last_state__state=IPResourceState.STATE_UP)
        if self.value() == "down":
            return queryset.exclude(last_state__state=IPResourceState.STATE_UP)
        if self.value() == "down-since":
            return queryset.filter(
                last_state__state=IPResourceState.STATE_DOWN,
                last_state_up__isnull=False,
            )
        if self.value() == "never-up":
            return queryset.filter(last_state_up__isnull=True)
        return None


class DeletableServiceFilter(admin.SimpleListFilter):
    title = "Supprimable"
    parameter_name = "deletable"

    def lookups(self, request, model_admin):
        return (
            (1, "Oui"),
            (0, "Non"),
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            return queryset.filter(long_stopped=False)
        if self.value() == "1":
            return queryset.filter(long_stopped=True)
        return None


class ActiveServiceFilter(admin.SimpleListFilter):
    title = "actif"
    parameter_name = "active"

    def lookups(self, request, model_admin):
        return (
            (1, "Actif"),
            (0, "Inactif"),
        )

    def queryset(self, request, queryset):
        if self.value() == "0":  # inactif
            return queryset.exclude(get_active_filter("allocation"))
        if self.value() == "1":  # actif
            return queryset.filter(get_active_filter("allocation"))
        return None


class RouteFilter(admin.SimpleListFilter):
    title = "route"
    parameter_name = "route"

    def lookups(self, request, model_admin):
        return (
            Route.objects.filter(
                allocation__in=ServiceAllocation.objects.filter(active=True),
            )
            .distinct()
            .values_list("pk", "name")
        )

    def queryset(self, request, queryset):
        try:
            route = int(self.value())
        except (TypeError, ValueError):
            pass
        else:
            allocations = ServiceAllocation.objects.filter(
                active=True,
                route__pk=route,
            ).values_list("pk", flat=True)
            queryset = queryset.filter(service_allocation__in=allocations)
        return queryset


class ActiveTunnelFilter(admin.SimpleListFilter):
    title = "status"
    parameter_name = "active"

    def lookups(self, request, model_admin):
        return (
            ("1", "Actif"),
            ("0", "Désactivé"),
        )

    def queryset(self, request, queryset):
        query = Q(ended__isnull=True)
        if self.value() == "0":
            return queryset.exclude(query)
        if self.value() == "1":
            return queryset.filter(query)
        return queryset


# ## Inlines


class AllocationInlineFormSet(BaseInlineFormSet):
    def save_new(self, form, commit=True):
        obj = super().save_new(form, commit)
        if type(obj) == ServiceAllocation:
            notify_allocation(self.request, obj)
        return obj

    def save_existing(self, form, instance, commit=True):
        old = type(instance).objects.get(pk=instance.pk)
        if type(instance) == ServiceAllocation:
            notify_allocation(self.request, instance, old)
        return super().save_existing(form, instance, commit)


class AllocationInline(admin.TabularInline):
    formset = AllocationInlineFormSet
    extra = 0
    show_change_link = True
    ordering = ("-start",)

    @admin.display(description="IP")
    def resource_link(self, obj):
        rurl = reverse("admin:services_ipresource_change", args=[obj.resource.pk])
        return format_html('<a href="{}">{}</a>', rurl, str(obj.resource))

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related("resource")

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        formset.request = request
        return formset

    def has_delete_permission(self, request, obj=None):
        return False


class NewAllocationMixin:
    verbose_name_plural = "Nouvelle allocation"
    max_num = 1

    def get_queryset(self, request):
        return super().get_queryset(request).model.objects.none()


class ActiveAllocationMixin:
    verbose_name_plural = "Allocations actives"
    max_num = 0

    def get_queryset(self, request):
        return super().get_queryset(request).filter(get_active_filter())


class InactiveAllocationMixin:
    verbose_name_plural = "Anciennes allocations"
    max_num = 0

    def get_queryset(self, request):
        return super().get_queryset(request).exclude(get_active_filter())


class ServiceAllocationMixin:
    model = ServiceAllocation
    fields = ("id", "service", "resource", "route", "start", "end")
    raw_id_fields = ("resource", "route")
    autocomplete_fields = ("service",)

    @admin.display(description="Service")
    def service_link(self, obj):
        rurl = reverse("admin:services_service_change", args=[obj.service.pk])
        return format_html('<a href="{}">{}</a>', rurl, str(obj.service))

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related("route")


class NewServiceAllocationInline(
    ServiceAllocationMixin,
    NewAllocationMixin,
    AllocationInline,
):
    fields = (
        "id",
        "service",
        "resource",
        "route",
    )


class ActiveServiceAllocationInline(
    ServiceAllocationMixin,
    ActiveAllocationMixin,
    AllocationInline,
):
    fields = (
        "id",
        "service_link",
        "resource_link",
        "route",
        "start",
        "stop",
    )
    readonly_fields = (
        "service_link",
        "start",
        "resource_link",
        "stop",
    )

    @admin.display(description="Terminer l`allocation")
    def stop(self, obj):
        return format_html(
            '<a href="{}" class="deletelink">Terminer</a>',
            reverse("admin:stop-allocation", kwargs={"resource": obj.resource.ip}),
        )


class InactiveServiceAllocationInline(
    ServiceAllocationMixin,
    InactiveAllocationMixin,
    AllocationInline,
):
    fields = ("id", "service_link", "resource_link", "route", "start", "end")
    readonly_fields = ("service_link", "resource_link", "route", "start", "end")


class PortInline(admin.TabularInline):
    model = Port
    max_num = 0

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class SwitchPortInline(PortInline):
    fields = (
        "port",
        "up",
        "reserved",
        "service",
        "notes",
    )
    readonly_fields = (
        "port",
        "up",
    )
    autocomplete_fields = ("service",)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related("switch", "service", "service__service_type")


class ServicePortInline(PortInline):
    fields = (
        "switch",
        "port",
        "up",
        "notes",
    )
    readonly_fields = (
        "switch",
        "port",
        "up",
    )


# ## Forms


class ServiceForm(ModelForm):
    def clean_adhesion(self):
        if (
            hasattr(self.instance, "adhesion")
            and self.instance.adhesion.pk != self.cleaned_data["adhesion"].pk
            and not self.instance.is_active()
        ):
            msg = (
                "Il n`est pas possible de ré-affecter à un autre adhérent un "
                "service inactif (i.e. sans allocations actives)."
            )
            raise ValidationError(
                msg,
            )
        return self.cleaned_data["adhesion"]


# ## ModelAdmin


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "get_adhesion_link",
        "get_adherent_link",
        "service_type",
        "label",
        "is_active",
    )
    list_select_related = (
        "adhesion",
        "adhesion__user",
        "adhesion__user__profile",
        "adhesion__corporation",
        "service_type",
    )
    list_filter = (
        ActiveServiceFilter,
        DeletableServiceFilter,
        "loan_equipment",
        ("service_type", admin.RelatedOnlyFieldListFilter),
    )
    search_fields = (
        "=id",
        "service_type__name",
        "label",
        "notes",
    )
    readonly_fields = (
        "get_contribution_link",
        "is_active",
    )
    raw_id_fields = ("adhesion",)
    form = ServiceForm

    def get_fields(self, request, obj=None):
        fields = ["adhesion", "service_type", "label"]
        if obj and obj.service_type.name == "wireguard":
            fields += ["wireguard_pubkey"]
        fields += ["notes", "loan_equipment", "get_contribution_link", "is_active"]
        return fields

    def save_model(self, request, srv, form, change):
        if srv.pk and "adhesion" in form.changed_data:
            with transaction.atomic():
                old_srv = Service.objects.get(pk=srv.pk)
                adhesion = srv.adhesion
                srv.adhesion = old_srv.adhesion
                label = srv.label
                srv.label = "%s (transféré à ADT%d le %s)" % (
                    srv.label,
                    adhesion.pk,
                    timezone.now().strftime("%d/%m/%Y"),
                )
                srv.save()
                new_srv = Service.objects.create(
                    adhesion=adhesion,
                    service_type=srv.service_type,
                    label=label,
                    notes=srv.notes,
                    loan_equipment=srv.loan_equipment,
                )
                for allocation in srv.active_allocations:
                    allocation.end = timezone.now()
                    allocation.save()
                    ServiceAllocation.objects.create(
                        resource=allocation.resource,
                        service=new_srv,
                        route=allocation.route,
                    )
        else:
            super().save_model(request, srv, form, change)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related("allocations")

    @admin.display(description=Adhesion.get_adhesion_link.short_description)
    def get_adhesion_link(self, service):
        return service.adhesion.get_adhesion_link()

    @admin.display(description=Adhesion.get_adherent_link.short_description)
    def get_adherent_link(self, service):
        return service.adhesion.get_adherent_link()

    @admin.display(description="Contribution financière")
    def get_contribution_link(self, obj):
        return format_html(
            '<a href="{}">{}</a>',
            obj.contribution.get_absolute_url(),
            obj.contribution.get_current_payment_display(),
        )

    def get_inline_instances(self, request, obj=None):
        inlines = []
        if obj and obj.ports.exists():
            inlines += [ServicePortInline]
        inlines += [NewServiceAllocationInline]
        if obj and obj.active_allocations.exists():
            inlines += [ActiveServiceAllocationInline]
        if obj and obj.inactive_allocations.exists():
            inlines += [InactiveServiceAllocationInline]
        return [inline(self.model, self.admin_site) for inline in inlines]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        if not obj or not obj.long_stopped:
            return False
        return True


@admin.register(IPPrefix)
class IPPrefixAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        # Interdiction de supprimer le préfix s`il est assigné à un tunnel
        return obj and obj.tunnel_set.exists()

    def has_change_permission(self, request, obj=None):
        return False if obj else True

    # pour embêcher de by-passer le check has_delete_permission, on désactive l`action
    # delete
    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions


@admin.register(IPResource)
class IPResourceAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "available_display",
        "route",
        "last_use",
        "ping",
    )
    list_filter = (
        "category",
        ResourceInUseFilter,
        ResourcePingFilter,
        "reserved",
        ("prefixes", admin.RelatedOnlyFieldListFilter),
        RouteFilter,
    )
    search_fields = ("=ip",)
    actions = ["contact_ip_owners"]
    ordering = ["ip"]

    def get_fields(self, request, obj=None):
        return self.get_readonly_fields(request, obj)

    def get_readonly_fields(self, request, obj=None):
        fields = ["ip", "ip6"]
        if obj:
            if obj.reserved:
                fields += ["reserved"]
            if not obj.in_use:
                fields += ["last_use"]
            if obj.last_state:
                fields += ["last_state"]
                if obj.last_state.state != IPResourceState.STATE_UP:
                    fields += ["last_state_up_end"]
            if obj.category == IPResource.CATEGORY_PUBLIC:
                fields += ["password"]
            if obj.checkmk_label:
                fields += ["checkmk"]
        return fields

    def get_inline_instances(self, request, obj=None):
        super_inlines = super().get_inline_instances(request, obj)
        inlines = []
        if obj and obj.category == IPResource.CATEGORY_PUBLIC:
            if obj.allocations.filter(get_active_filter()).exists():
                inlines += [ActiveServiceAllocationInline]
            else:
                inlines += [NewServiceAllocationInline]
            if obj.allocations.exclude(get_active_filter()).exists():
                inlines += [InactiveServiceAllocationInline]
        return [
            inline(self.model, self.admin_site) for inline in inlines
        ] + super_inlines

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related(
            "last_state",
            "last_state__end",
            "last_state_up",
            "last_state_up__end",
        )
        now = timezone.now()
        qs = qs.annotate(
            last_use=models.Case(
                models.When(in_use=True, then=now),
                models.When(category=0, then=models.Max("service_allocation__end")),
                default=None,
            ),
        )
        qs = qs.annotate(
            downtime=ExpressionWrapper(
                models.Value(now) - models.F("last_state_up__end__date"),
                output_field=models.DurationField(),
            ),
        )
        return qs.annotate(
            route=models.Case(
                models.When(
                    in_use=True,
                    then=models.Subquery(
                        ServiceAllocation.objects.filter(
                            Q(resource=models.OuterRef("pk")) & get_active_filter(),
                        ).values("route__name")[:1],
                    ),
                ),
                output_field=models.CharField(),
            ),
        )

    @admin.display(description="Disponible", boolean=True)
    def available_display(self, obj):
        if obj.category == IPResource.CATEGORY_PUBLIC:
            return not obj.reserved and not obj.in_use
        return None  # this should not being used for antenna

    @admin.display(description="Dernière utilisation", ordering="last_use")
    def last_use(self, obj):
        if obj.last_use:
            return naturaltime(obj.last_use)
        return "-"

    @admin.display(description="dernier état UP")
    def last_state_up_end(self, obj):
        return naturaltime(obj.last_state_up.end.date)

    @admin.display(description="ping", ordering="downtime")
    def ping(self, obj):
        if not obj.last_state:
            label = "Inconnu"
        elif obj.last_state.state == IPResourceState.STATE_UP:
            label = (
                "UP (dernière vérification : "
                + naturaltime(obj.last_state.end.date)
                + ")"
            )
        elif obj.last_state_up:
            label = (
                "DOWN (dernier ping : " + naturaltime(obj.last_state_up.end.date) + ")"
            )
        else:
            label = (
                "DOWN (depuis au moins : "
                + naturaltime(obj.last_state.begin.date)
                + ")"
            )
        if obj.checkmk_url:
            return format_html('<a href="{}">{}</a>', obj.checkmk_url, label)
        return label

    @admin.display(description="route", ordering="route")
    def route(self, obj):
        return obj.route

    @admin.display(description="Préfixe IPv6", ordering="ip")
    def ip6(self, obj):
        return ipv4_to_ipv6(IPv4Address(obj.ip))

    @admin.display(description="CheckMK")
    def checkmk(self, obj):
        return format_html('<a href="{}">{}</a>', obj.checkmk_url, "voir")

    @admin.action(description="Contacter les adhérents")
    def contact_ip_owners(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        services = (
            ServiceAllocation.objects.filter(resource__ip__in=selected)
            .filter(get_active_filter())
            .values_list("service__adhesion", flat=True)
        )
        pk = ",".join(map(str, services))
        return HttpResponseRedirect(reverse("admin:contact-adherents") + "?pk=%s" % pk)

    def stop_allocation(self, request, resource):
        resource = self.get_object(request, resource)
        allocation = resource.allocations.filter(get_active_filter()).first()
        if not allocation:  # L`IP n`est pas allouée
            return HttpResponseRedirect(
                reverse("admin:services_ipresource_change", args=[resource.pk]),
            )
        form = StopAllocationForm(request.POST or None)
        if request.method == "POST" and form.is_valid():
            self.message_user(request, "Allocation stoppée.")
            allocation.end = timezone.now()
            allocation.save()
            notify_allocation(request, allocation)
            # Il faudrait rajouter un redirect dans l`URL pour rediriger vers l`IP ou le
            # Service
            return HttpResponseRedirect(
                reverse("admin:services_ipresource_change", args=[resource.pk]),
            )
        context = self.admin_site.each_context(request)
        context.update(
            {
                "opts": self.model._meta,
                "title": "Stopper une allocation",
                "object": resource,
                "media": self.media,
                "form": form,
            },
        )
        return TemplateResponse(
            request,
            "admin/services/ipresource/stop_allocation.html",
            context,
        )

    def get_urls(self):
        my_urls = [
            path(
                "<resource>/stop/",
                self.admin_site.admin_view(self.stop_allocation),
                name="stop-allocation",
            ),
        ]
        return my_urls + super().get_urls()

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(IPResourceState)
class IPResourceStateAdmin(admin.ModelAdmin):
    list_display = ("ip", "state", "begin", "end")
    search_fields = ["=ip"]
    ordering = ["-begin"]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ("name", "get_ip_count", "get_adh_count")
    list_filter = (RouteInUseFilter, RouteTypeFilter)
    search_fields = ("name",)

    @admin.display(description="Nombre d’IP")
    def get_ip_count(self, obj):
        return obj.get_ip().count()

    @admin.display(description="Nombre d’adhésions")
    def get_adh_count(self, obj):
        return obj.get_adh().count()

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (
                (None, {"fields": ["name"]}),
                ("Adhérent·e·s", {"fields": ["get_adh"], "classes": ["collapse"]}),
                ("E-mails", {"fields": ["get_email"], "classes": ["collapse"]}),
                ("SMS", {"fields": ["get_sms"], "classes": ["collapse"]}),
                ("IP", {"fields": ["get_ip"], "classes": ["collapse"]}),
            )
        return ((None, {"fields": ["name"]}),)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return (
                "get_email",
                "get_sms",
                "get_ip",
                "get_adh",
            )
        return ()

    @admin.display(description="E-mails")
    def get_email(self, route):
        return "\n".join(route.get_email())

    @admin.display(description="SMS")
    def get_sms(self, route):
        def sms_filter(x):
            return x[:2] == "06" or x[:2] == "07" or x[:3] == "+336" or x[:3] == "+337"

        return "\n".join(filter(sms_filter, route.get_tel()))

    @admin.display(description="IP")
    def get_ip(self, route):
        return "\n".join(route.get_ip())

    @admin.display(description="Adhérent·e·s")
    def get_adh(self, route):
        return "\n".join(f"{adh} {adh.adherent}" for adh in route.get_adh())

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        if obj:
            if obj.allocations.exists():
                return False
            return True
        return False


@admin.register(Tunnel)
class TunnelAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "created", "active")
    list_filter = (ActiveTunnelFilter,)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        if obj:
            if obj.allocations.exists():
                return False
            return True
        return False

    @admin.display(description="Actif", boolean=True)
    def active(self, obj):
        return not obj.ended


@admin.register(ServiceType)
class ServiceTypeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "contact",
    )
    fields = (
        "name",
        "contact",
    )

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Switch)
class SwitchAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "ports_count",
        "active_ports_count",
        "inactive_ports_count",
        "unknown_ports_count",
    )
    fields = (
        "name",
        "first_port",
        "last_port",
        "notes",
    )
    search_fields = (
        "name",
        "notes",
        "ports__notes",
        "ports__service__label",
    )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.annotate(
            active_ports_count=models.Count(
                models.Case(models.When(ports__up=True, then=models.Value("1"))),
            ),
            unknown_ports_count=models.Count(
                models.Case(
                    models.When(ports__up__isnull=True, then=models.Value("1")),
                ),
            ),
            inactive_ports_count=models.Count(
                models.Case(models.When(ports__up=False, then=models.Value("1"))),
            ),
        )

    @admin.display(description="Nombre de ports")
    def ports_count(self, switch):
        return switch.last_port - switch.first_port + 1

    @admin.display(description="up", ordering="active_ports_count")
    def active_ports_count(self, switch):
        return switch.active_ports_count

    @admin.display(description="down", ordering="inactive_ports_count")
    def inactive_ports_count(self, switch):
        return switch.inactive_ports_count

    @admin.display(description="inconnus", ordering="unknown_ports_count")
    def unknown_ports_count(self, switch):
        return switch.unknown_ports_count

    def get_inline_instances(self, request, obj=None):
        return [SwitchPortInline(self.model, self.admin_site)] if obj else []

    def get_readonly_fields(self, request, obj=None):
        return ("first_port", "last_port") if obj else ()

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        return False
