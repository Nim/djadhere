from django.core.management.base import BaseCommand

from services.utils.fastpinger import fastpinger_update


class Command(BaseCommand):
    help = "Analyse d`un fichier fastpinger"

    def add_arguments(self, parser):
        parser.add_argument("file", help="Fichier fastpinger")

    def handle(self, *args, **options):
        with open(options["file"], "rb") as f:
            stats = fastpinger_update(f)
        self.stdout.write(stats + "\n")
