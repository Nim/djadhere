from django.core.management.base import BaseCommand

from services.utils.architecture import export_ip, export_switch


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--switch", action="store_true")
        parser.add_argument("--ip", action="store_true")

    def handle(self, *args, **options):
        if options["switch"]:
            self.export_switch()
        if options["ip"]:
            self.export_ip()

    def export_switch(self):
        print(export_switch())

    def export_ip(self):
        print(export_ip())
