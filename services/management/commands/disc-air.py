# ruff: noqa: N999
import json
import os

from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand

import yaml

from djadhere.utils import get_active_filter
from services.models import Antenna, AntennaAllocation, Tunnel


class Command(BaseCommand):
    help = "Import des informatios du cache de disc-air"

    def add_arguments(self, parser):
        parser.add_argument("disc-air", help="Cache disc-air")

    def handle(self, *args, **options):
        cache = options["disc-air"]
        for tunnel in Tunnel.objects.all():
            if not os.path.isdir(os.path.join(cache, tunnel.name)):
                continue
            gps_file = os.path.join(cache, "..", "gps-%s.yaml" % tunnel.name)
            if os.path.isfile(gps_file):
                with open(gps_file) as fd:
                    gps = yaml.load(fd)
            for network in tunnel.networks.all():
                for ip in network.ipresource_set.all():
                    status_file = os.path.join(
                        cache,
                        tunnel.name,
                        ip.ip,
                        "fd694727ed4810b9d242c45906ea9417",
                    )
                    if not os.path.isfile(status_file):
                        continue
                    with open(status_file) as fd:
                        try:
                            status = json.load(fd)
                        except Exception:
                            continue
                    try:
                        hostname = status["host"]["hostname"]
                        mode = status["wireless"]["mode"]
                        ssid = status["wireless"]["essid"]
                        mac = status["wireless"]["apmac"]
                    except KeyError:
                        continue
                    mode = 1 if mode == "ap" else 2
                    if ip.in_use:
                        update = False
                        antenna = (
                            ip.allocations.filter(get_active_filter()).first().antenna
                        )  # peut faire mieux …
                        if antenna.label:
                            if antenna.label != hostname:
                                # print(
                                # ip,
                                # ", djadhere:",
                                # antenna.label,
                                # ", disc-air:",
                                # hostname,
                                # )
                                continue
                        else:
                            # print("update hostname")
                            antenna.label = hostname
                            update = True
                        if antenna.mode:
                            if antenna.mode != mode:
                                # print(
                                # ip, ", djadhere:", antenna.mode, ", disc-air:", mode
                                # )
                                continue
                        else:
                            # print("update mode")
                            antenna.mode = mode
                            update = True
                        if antenna.ssid:
                            if antenna.ssid != ssid:
                                # print(
                                # ip, ", djadhere:", antenna.ssid, ", disc-air:", ssid
                                # )
                                continue
                        else:
                            # print("update ssid")
                            antenna.ssid = ssid
                            update = True
                        if antenna.mac:
                            if antenna.mac != mac:
                                # print(
                                # ip, ", djadhere:", antenna.mac, ", disc-air:", mac
                                # )
                                continue
                        else:
                            # print("update mac")
                            antenna.mac = mac
                            update = True
                        if ip.ip in gps:
                            coords = gps[ip.ip]
                            if len(coords) == 3:
                                latitude, longitude, orientation = coords
                            else:
                                latitude, longitude = coords
                                orientation = None
                            assert 41 < latitude and latitude < 45
                            assert 1 < longitude and longitude < 2
                            position = Point(longitude, latitude, srid=4326)
                            if antenna.position:
                                if antenna.position != position:
                                    # print(
                                    # ip,
                                    # ", djadhere:",
                                    # antenna.position,
                                    # ", disc-air:",
                                    # position,
                                    # )
                                    continue
                            else:
                                antenna.position = position
                                update = True
                            if antenna.orientation:
                                if antenna.orientation != orientation:
                                    # print(antenna.orientation, orientation)
                                    continue
                            elif orientation:
                                antenna.orientation = orientation
                                update = True
                        if update:
                            antenna.save()
                    else:
                        # print("created")
                        antenna = Antenna.objects.create(
                            label=hostname,
                            mode=mode,
                            ssid=ssid,
                            mac=mac,
                        )
                        AntennaAllocation.objects.create(resource=ip, antenna=antenna)
