import re

from django.core.management.base import BaseCommand

from djadhere.utils import from_livestatus
from services.models import Switch


class Command(BaseCommand):
    help = "Récupération du dernier ping depuis check_mk"

    def add_arguments(self, parser):
        parser.add_argument("--switch", nargs="+")

    def handle(self, *args, **options):
        dell_iface_regex = re.compile(
            r"^Interface TenGigabitEthernet 0/(?P<id>[0-9]+)$",
        )
        ubnt_iface_regex = re.compile(
            r"^Interface Slot: 0 Port: (?P<id>[0-9]+) (Gigabit|10G) - Level$",
        )
        nexus_iface_regex = re.compile(r"^Interface Ethernet1/(?P<id>[0-9]+)$")
        status_regex = re.compile(
            r"\w+ - \[(?P<label>[^\]]+)\] \((?P<status>up|down|admin down)\)",
        )
        if options["switch"]:
            switch_list = Switch.objects.filter(name__in=options["switch"])
        else:
            switch_list = Switch.objects.all()
        for sw in switch_list:
            up_count, down_count, unknown_count = (
                sw.ports.filter(up=True).count(),
                sw.ports.filter(up=False).count(),
                sw.ports.filter(up__isnull=True).count(),
            )
            up, down = [], []
            hosts = from_livestatus(
                "hosts",
                query=["Filter: host_name = %s" % sw.name],
                columns=["services_with_info"],
            )
            if len(hosts) != 1:
                continue
            host = hosts[0]
            for service in host.services_with_info:
                description, _, _, info = service
                g = dell_iface_regex.match(description)
                if not g:
                    g = ubnt_iface_regex.match(description)
                if not g:
                    g = nexus_iface_regex.match(description)
                if not g:
                    continue
                port = int(g.group("id"))
                if port < sw.first_port or port > sw.last_port:
                    continue
                g = status_regex.match(info)
                if not g:
                    self.stdout.write(
                        self.style.WARNING(
                            "Switch %s port %d status unknown: %s"
                            % (sw.name, port, info),
                        ),
                    )
                    continue
                status = g.group("status")
                if status == "up":
                    up.append(port)
                else:
                    assert status == "down" or status == "admin down"
                    down.append(port)
            unknown = set(range(sw.first_port, sw.last_port + 1)) - set(up) - set(down)
            sw.ports.filter(port__in=up).exclude(up=True).update(up=True)
            sw.ports.filter(port__in=down).exclude(up=False).update(up=False)
            sw.ports.filter(port__in=unknown).exclude(up__isnull=True).update(up=None)
            upped, downed, unknowned = (
                len(up) - up_count,
                len(down) - down_count,
                len(unknown) - unknown_count,
            )
            if upped or downed or unknowned:
                self.stdout.write(
                    "Switch %s: UP: %d (%+d), DOWN: %d (%+d), UNKNOWN: %d (%+d)"
                    % (
                        sw.name,
                        len(up),
                        upped,
                        len(down),
                        downed,
                        len(unknown),
                        unknowned,
                    ),
                )
