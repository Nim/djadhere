# Generated by Django 1.11 on 2017-06-12 11:39

from django.db import migrations, models
import services.models


class Migration(migrations.Migration):
    dependencies = [
        ("services", "0029_route"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="ipprefix",
            options={
                "ordering": ["prefix"],
                "verbose_name": "Réseau",
                "verbose_name_plural": "Réseaux",
            },
        ),
        migrations.AlterField(
            model_name="ipprefix",
            name="prefix",
            field=models.CharField(
                max_length=128,
                unique=True,
                validators=[services.models.ipprefix_validator],
                verbose_name="Préfixe",
            ),
        ),
    ]
