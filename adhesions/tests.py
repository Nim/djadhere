from datetime import datetime, timedelta
from io import StringIO

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse

from banking.models import PaymentUpdate, RecurringPayment
from services.models import IPResource, Route, Service, ServiceAllocation, ServiceType

from .models import Adhesion, Corporation, User


class AdhesionsMixin:
    def setUp(self):
        User.objects.create_user(
            "admin",
            email="admin@example.net",
            password="admin",
            is_staff=True,
            is_superuser=True,
        )
        user = User.objects.create_user(
            "user",
            first_name="first",
            last_name="last",
            email="user@example.net",
            password="user",
        )
        corp1 = Corporation.objects.create(social_reason="GoodCorp")
        corp1.members.add(user)
        Corporation.objects.create(social_reason="EvilCorp")
        # Adhesion.objects.create(corporation=corp1)


class ModelsTest(AdhesionsMixin, TestCase):
    def test_user(self):
        user = User.objects.get(username="admin")
        profile = user.profile
        profile.phone_number = "0123456789"
        profile.address = "1 Av. du A"
        profile.ssh_keys = "ssh-ed25519 anuristanuietaunirestauie"
        profile.save()
        user = User.objects.get(username="admin")
        self.assertEqual(user.adhesions.first(), Adhesion.objects.get(user=user))
        self.assertEqual(user.phone_number, "0123456789")
        self.assertEqual(user.address, "1 Av. du A")
        self.assertEqual(str(user), "admin")


class AdminTest(AdhesionsMixin, TestCase):
    def test_admin_views(self):
        self.client.login(username="admin", password="admin")
        response = self.client.get(reverse("admin:adhesions_user_changelist"))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("admin:adhesions_corporation_changelist"))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("admin:adhesions_adhesion_changelist"))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            reverse(
                "admin:adhesions_user_change",
                args=[User.objects.get(username="user").pk],
            ),
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            reverse(
                "admin:adhesions_corporation_change",
                args=[Corporation.objects.get(social_reason="GoodCorp").pk],
            ),
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            reverse(
                "admin:adhesions_adhesion_change",
                args=[User.objects.get(username="user").adhesion.pk],
            ),
        )
        self.assertEqual(response.status_code, 200)

    def test_user_creation(self):
        self.assertEqual(User.objects.count(), 2)
        self.client.login(username="admin", password="admin")
        response = self.client.get(reverse("admin:adhesions_user_add"))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(
            reverse("admin:adhesions_user_add"),
            {
                "first_name": "first",
                "last_name": "last",
                "username": "",
                "email": "added@example.org",
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(User.objects.count(), 3)


class ViewsTestCase(AdhesionsMixin, TestCase):
    def test_adhesion_backend(self):
        user = User.objects.get(username="user")
        adhesion = user.adhesion
        self.assertFalse(
            self.client.login(username="%d" % adhesion.pk, password="wrong"),
        )
        self.assertFalse(
            self.client.login(username="ADT%d" % adhesion.pk, password="wrong"),
        )
        self.assertFalse(self.client.login(username="9999", password="user"))
        self.assertFalse(self.client.login(username="ADT9999", password="user"))
        self.assertTrue(self.client.login(username="%d" % adhesion.pk, password="user"))
        self.assertTrue(
            self.client.login(username="ADT%d" % adhesion.pk, password="user"),
        )

    def test_user(self):
        response = self.client.get(reverse("adhesion-detail"))
        self.assertRedirects(
            response,
            reverse("login") + "?next=" + reverse("adhesion-detail"),
        )
        self.client.login(username="user", password="user")
        response = self.client.get(reverse("adhesion-detail"))
        self.assertEqual(response.status_code, 200)

    def test_corporation_menu(self):
        self.client.login(username="user", password="user")
        response = self.client.get("/")
        self.assertContains(response, "GoodCorp")
        self.assertNotContains(response, "EvilCorp")

    def test_adhesions_urls(self):
        self.client.login(username="admin", password="admin")
        corp1 = Corporation.objects.first()
        response = self.client.get(
            reverse("corporation-detail", kwargs={"pk": corp1.id}),
        )
        self.assertEqual(response.status_code, 200)

    def test_adhesion_model(self):
        user = User.objects.get(username="user")
        adh1 = Adhesion.objects.get(user=user)
        self.assertEqual(user.adhesion, adh1)
        self.assertTrue(adh1.is_physical())
        self.assertFalse(adh1.is_moral())
        self.assertEqual(str(adh1.user), "first last")
        corp = Corporation.objects.get(social_reason="GoodCorp")
        adh2 = Adhesion.objects.get(corporation=corp)
        self.assertEqual(corp.adhesion, adh2)
        self.assertFalse(adh2.is_physical())
        self.assertTrue(adh2.is_moral())
        self.assertEqual(str(adh2.corporation), "GoodCorp")

    def test_corporation_normal_user(self):
        self.client.login(username="user", password="user")
        cor = Corporation.objects.first()
        response = self.client.get(reverse("corporation-detail", kwargs={"pk": cor.id}))
        self.assertEqual(response.status_code, 200)

    def test_corporation_admin(self):
        self.client.login(username="user", password="user")
        cor = Corporation.objects.first()
        response = self.client.get(reverse("corporation-detail", kwargs={"pk": cor.id}))
        self.assertEqual(response.status_code, 200)

    def test_corporation_wrong_user(self):
        User.objects.create_user(
            "bad_user",
            first_name="bad_first",
            last_name="bad_last",
            email="bad_user@example.net",
            password="bad_user",
        )
        self.client.login(username="bad_user", password="bad_user")
        cor = Corporation.objects.first()
        response = self.client.get(reverse("corporation-detail", kwargs={"pk": cor.id}))
        self.assertEqual(response.status_code, 403)


class CommandsTestCase(AdhesionsMixin, TestCase):
    def test_list(self):
        out = StringIO()
        call_command("adherents", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "first last")
        self.assertRegex(result, "GoodCorp")

        # les adhésions sont crées par des signaux maintenant
        # du coup tous les user sont adhérents, même evilcorp et admin.
        # TODO: est-ce que c`est un problème, ou est-ce qu`on vire ça ?
        # self.assertNotRegex(result, "admin")  # non adhérent
        # self.assertNotRegex(result, "EvilCorp")  # non adhérent

        out = StringIO()
        call_command("adherents", "--physique", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "first last")
        self.assertNotRegex(result, "GoodCorp")

        out = StringIO()
        call_command("adherents", "--morale", stdout=out)
        result = out.getvalue()
        self.assertNotRegex(result, "first last")
        self.assertRegex(result, "GoodCorp")


class DeletableTestCase(TestCase):
    def test_deletable(self):
        one_year_ago = datetime.now() - timedelta(days=365)
        two_years_ago = datetime.now() - timedelta(days=365 * 2)
        vm = ServiceType.objects.create(name="VM")
        User.objects.create_user(
            "admin",
            password="admin",
            is_staff=True,
            is_superuser=True,
        )
        user = User.objects.create(username="user", password="user")

        # user just created. it should already have an adhesion. Admin too.
        self.assertEqual(Adhesion.objects.count(), 2)

        # this adhesion has no membership yet. it should not be "long stopped"
        self.assertFalse(Adhesion.objects.filter(long_stopped=True).exists())

        # this user started a membership 2y ago, they are still not "long stopped"
        membership = RecurringPayment.objects.create()
        PaymentUpdate.objects.create(
            payment=membership,
            amount=20,
            period=12,
            start=two_years_ago,
            validated=True,
        )
        user.adhesion.membership = membership
        user.adhesion.save()
        self.assertFalse(Adhesion.objects.filter(long_stopped=True).exists())

        # unless they stopped paying more than a year ago
        PaymentUpdate.objects.create(
            payment=membership,
            amount=0,
            period=1,
            payment_method=PaymentUpdate.STOP,
            start=one_year_ago - timedelta(days=10),
            validated=True,
        )
        self.assertTrue(Adhesion.objects.filter(long_stopped=True).exists())

        # unless they had a service 2y ago
        service = Service.objects.create(
            adhesion=user.adhesion,
            service_type=vm,
            label="service",
        )
        self.assertFalse(Adhesion.objects.filter(long_stopped=True).exists())

        # especially if they were paying for it
        contribution = RecurringPayment.objects.create()
        PaymentUpdate.objects.create(
            payment=contribution,
            amount=5,
            period=1,
            start=two_years_ago,
            validated=True,
        )
        service.contribution = contribution
        service.save()
        self.assertFalse(Adhesion.objects.filter(long_stopped=True).exists())

        # except if they stopped paying mor than a year ago
        PaymentUpdate.objects.create(
            payment=contribution,
            amount=0,
            period=1,
            payment_method=PaymentUpdate.STOP,
            start=one_year_ago - timedelta(days=10),
            validated=True,
        )
        self.assertTrue(Adhesion.objects.filter(long_stopped=True).exists())

        # except if that service had ressources
        ip = IPResource.objects.create(
            ip="91.224.148.168",
            category=IPResource.CATEGORY_PUBLIC,
        )
        route = Route.objects.create(name="openstack")
        allocation = ServiceAllocation.objects.create(
            start=two_years_ago,
            resource=ip,
            service=service,
            route=route,
        )
        self.assertFalse(Adhesion.objects.filter(long_stopped=True).exists())

        # except if those ressources were freed
        allocation.end = one_year_ago - timedelta(days=10)
        allocation.save()
        self.assertTrue(Adhesion.objects.filter(long_stopped=True).exists())
