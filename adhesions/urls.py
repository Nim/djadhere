from django.urls import path

from . import views

urlpatterns = [
    path("", views.user, name="adhesion-detail"),
    path("asso/<int:pk>/", views.corporation, name="corporation-detail"),
]
