from django.apps import AppConfig


class AdhesionsConfig(AppConfig):
    name = "adhesions"
    verbose_name = "Adhérent·e·s"

    def ready(self):
        import adhesions.signals  # noqa
