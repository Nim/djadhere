from django.core import serializers

from adhesions.models import Corporation


class CorporationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            if "corporations" not in request.session:
                corporations = Corporation.objects.filter(members=request.user)
                request.session["corporations"] = serializers.serialize(
                    "json",
                    corporations,
                    fields=(
                        "pk",
                        "social_reason",
                    ),
                )
            request.corporations = serializers.deserialize(
                "json",
                request.session["corporations"],
            )
        return self.get_response(request)
