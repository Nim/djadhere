# Generated by Django 1.10.3 on 2016-12-30 17:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("contenttypes", "0002_remove_content_type_name"),
        ("adhesions", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="adherent",
            name="contribution",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="adherent",
                to="banking.Payment",
                verbose_name="Cotisation",
            ),
        ),
        migrations.AlterUniqueTogether(
            name="adherent",
            unique_together={("adherent_type", "adherent_id")},
        ),
    ]
