# Generated by Django 1.11.2 on 2017-06-14 21:12

from django.db import migrations


def create_missing_adhesions(apps, schema_editor):
    User = apps.get_model("adhesions", "User")
    Corporation = apps.get_model("adhesions", "Corporation")
    Adhesion = apps.get_model("adhesions", "Adhesion")
    ContentType = apps.get_model("contenttypes", "ContentType")
    user_type = ContentType.objects.get_for_model(User)
    corp_type = ContentType.objects.get_for_model(Corporation)
    db_alias = schema_editor.connection.alias
    for user in User.objects.using(db_alias).filter(adhesion__isnull=True).all():
        if not Adhesion.objects.using(db_alias).filter(user__pk=user.pk).exists():
            Adhesion.objects.using(db_alias).create(user=user)
    for corp in Corporation.objects.using(db_alias).filter(adhesion__isnull=True).all():
        if (
            not Adhesion.objects.using(db_alias)
            .filter(corporation__pk=corp.pk)
            .exists()
        ):
            Adhesion.objects.using(db_alias).create(corporation=corp)


class Migration(migrations.Migration):
    dependencies = [
        ("adhesions", "0014_auto_20170614_2242"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="adhesion",
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name="adhesion",
            name="adherent_id",
        ),
        migrations.RemoveField(
            model_name="adhesion",
            name="adherent_type",
        ),
        migrations.RunPython(create_missing_adhesions),
    ]
