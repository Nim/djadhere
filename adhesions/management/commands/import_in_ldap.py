from base64 import b64decode, b64encode

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

import ldap3
from ldap3 import ALL, ALL_ATTRIBUTES, LEVEL, MODIFY_REPLACE, Connection, Server


def django_password_to_ldap(password):
    algorithm, iterations, salt, hash = password.split("$", 3)
    if algorithm != "pbkdf2_sha256_dk256":
        return None
    password = int(iterations).to_bytes(4, byteorder="big")
    if len(salt) != 64:
        return None
    password += salt.encode("ascii")
    hash = b64decode(hash)
    if len(hash) != 256:
        return None
    password += hash
    return "{PBKDF2_SHA256}" + b64encode(password).decode("ascii")


class Command(BaseCommand):
    help = "Exporter la base des utilisateurs vers un serveur LDAP."

    def add_arguments(self, parser):
        pass

    def update_ldap_entry(self, conn, dn, attribute, value):
        mod = {attribute: [(MODIFY_REPLACE, [value])]}
        conn.modify(dn, mod)
        if conn.result["result"] != 0:
            self.stdout.write(
                self.style.ERROR(
                    "{} -> {}: {}".format(dn, mod, conn.result["description"]),
                ),
            )
        return

    def handle(self, *args, **options):
        ldap3.set_config_parameter("DEFAULT_SERVER_ENCODING", "utf-8")
        ldap3.set_config_parameter("DEFAULT_CLIENT_ENCODING", "utf-8")
        server = Server(settings.LDAP_HOST, use_ssl=True, get_info=ALL)
        with Connection(
            server,
            user=settings.LDAP_MANAGER,
            password=settings.LDAP_PASSWORD,
            #auto_bind=ldap3.AUTO_BIND_TLS_BEFORE_BIND,
            auto_bind=True,
        ) as conn:
            for user in User.objects.all():
                conn.search(
                    search_base=settings.LDAP_USERS_BASE,
                    search_filter="(&(objectClass=inetOrgPerson)(uid={}))".format(
                        user.adhesion.pk,
                    ),
                    search_scope=LEVEL,
                    attributes=ALL_ATTRIBUTES,
                )

                ldap_password = django_password_to_ldap(user.password)
                if (
                    not ldap_password
                ):  # user must login on django to update its password
                    continue

                if conn.entries:  # user already present in ldap
                    assert len(conn.entries) == 1
                    (ldap_user,) = conn.entries
                    user_dn = f"uid={ldap_user.uid.value},{settings.LDAP_USERS_BASE}"
                    if ldap_password != ldap_user.userPassword.value.decode("utf-8"):
                        self.stdout.write(
                            self.style.ERROR(
                                "userPassword for user {} has changed.".format(
                                    ldap_user.cn.value,
                                ),
                            ),
                        )
                        self.update_ldap_entry(
                            conn,
                            user_dn,
                            "userPassword",
                            ldap_password,
                        )
                    if user.username != ldap_user.cn.value:
                        self.stdout.write(
                            self.style.ERROR(
                                "cn for user {} has changed {}.".format(
                                    user.adhesion,
                                    user.username,
                                ),
                            ),
                        )
                        self.update_ldap_entry(conn, user_dn, "cn", user.username)
                    if user.last_name and (
                        not hasattr(ldap_user, "sn")
                        or user.last_name != ldap_user.sn.value
                    ):
                        self.stdout.write(
                            self.style.ERROR(
                                "sn for user {} has changed: {}.".format(
                                    user.adhesion,
                                    user.last_name,
                                ),
                            ),
                        )
                        self.update_ldap_entry(conn, user_dn, "sn", user.last_name)
                    if user.first_name and (
                        not hasattr(ldap_user, "givenName")
                        or user.first_name != ldap_user.givenName.value
                    ):
                        self.stdout.write(
                            self.style.ERROR(
                                "givenName for user {} has changed: {}.".format(
                                    user.adhesion,
                                    user.first_name,
                                ),
                            ),
                        )
                        self.update_ldap_entry(
                            conn,
                            user_dn,
                            "givenName",
                            user.first_name,
                        )
                    if user.email and (
                        not hasattr(ldap_user, "mail")
                        or user.email != ldap_user.mail.value
                    ):
                        self.stdout.write(
                            self.style.ERROR(
                                "email for user {} has changed: {}.".format(
                                    user.adhesion,
                                    user.email,
                                ),
                            ),
                        )
                        self.update_ldap_entry(conn, user_dn, "mail", user.email)
                else:  # user not yet present in ldap
                    attributes = {
                        "objectClass": [
                            "inetOrgPerson",
                            "organizationalPerson",
                            "person",
                            "top",
                        ],
                        "displayName": str(user),
                        "cn": user.username,
                        "sn": str(user),  # default if user.last_name is empty
                        "userPassword": ldap_password,
                    }
                    if user.email:
                        attributes["mail"] = user.email
                    if user.first_name and user.first_name != "FIXME":
                        attributes["givenName"] = user.first_name
                    if user.last_name and user.last_name != str(user.adhesion.pk):
                        attributes["sn"] = user.last_name

                    conn.add(
                        dn=f"uid={user.adhesion.pk},{settings.LDAP_USERS_BASE}",
                        attributes=attributes,
                    )
                    if conn.result["result"] == 0:
                        self.stdout.write(
                            self.style.SUCCESS(f"{user.adhesion} imported in LDAP."),
                        )
                    else:
                        self.stdout.write(
                            self.style.ERROR(
                                f"Error when importing {user.adhesion} in LDAP.",
                            ),
                        )
                        self.stdout.write(str(conn.result))
