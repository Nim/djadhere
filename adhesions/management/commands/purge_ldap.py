from django.conf import settings
from django.core.management.base import BaseCommand

import ldap3
from ldap3 import ALL, ALL_ATTRIBUTES, LEVEL, Connection, Server

from adhesions.models import Adhesion


class Command(BaseCommand):
    help = "Supprimer de la base LDAP les comptes inactifs ou supprimés de Djadhère."

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        ldap3.set_config_parameter("DEFAULT_SERVER_ENCODING", "utf-8")
        ldap3.set_config_parameter("DEFAULT_CLIENT_ENCODING", "utf-8")
        server = Server(settings.LDAP_HOST, use_ssl=True, get_info=ALL)
        with Connection(
            server,
            user=settings.LDAP_MANAGER,
            password=settings.LDAP_PASSWORD,
            auto_bind=ldap3.AUTO_BIND_TLS_BEFORE_BIND,
        ) as conn:
            conn.search(
                search_base=settings.LDAP_USERS_BASE,
                search_filter="(objectClass=inetOrgPerson)",
                search_scope=LEVEL,
                attributes=ALL_ATTRIBUTES,
            )
            self.stdout.write(
                self.style.SUCCESS(f"{len(conn.entries)} users founnd in LDAP."),
            )
            for ldap_user in conn.entries:
                delete = False
                # print(ldap_user.uid.value)
                try:
                    adh = Adhesion.objects.get(pk=ldap_user.uid.value)
                except Adhesion.DoesNotExist:
                    self.stdout.write(
                        self.style.ERROR(
                            f"Adhesion ADT{ldap_user.uid.value} inexistante",
                        ),
                    )
                    delete = True
                # if adh.active is None:
                # self.stdout.write(
                # self.style.ERROR(
                # "Adhesion ADT{} {} unknown".format(
                # adh.pk, ldap_user.cn.value
                # )
                # )
                # )
                # delete = False
                if not adh.active:
                    self.stdout.write(
                        self.style.ERROR(
                            f"Adhesion ADT{adh.pk} {ldap_user.cn.value} inactive",
                        ),
                    )
                    delete = True
                if delete:
                    conn.delete(ldap_user.entry_dn)
                    if conn.result["result"] != 0:
                        self.stdout.write(
                            self.style.ERROR(
                                "{}: {}".format(
                                    ldap_user.entry_dn,
                                    conn.result["description"],
                                ),
                            ),
                        )
