from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils.timezone import now

from adhesions.models import Adhesion


class Command(BaseCommand):
    help = "Lister les adhérents"

    def add_arguments(self, parser):
        parser.add_argument(
            "--emails",
            action="store_true",
            help="Afficher le courriel des adhérents.",
        )
        parser.add_argument(
            "--newer-than",
            type=int,
            help="Afficher les adhérents inscrit depuis moins "
            "d`un certain nombre de jours.",
        )
        moral_group = parser.add_mutually_exclusive_group()
        moral_group.add_argument(
            "--physique",
            action="store_true",
            help="Afficher uniquement les personnes physiques",
        )
        moral_group.add_argument(
            "--morale",
            action="store_true",
            help="Afficher uniquement les personnes morales",
        )
        act_group = parser.add_mutually_exclusive_group()
        act_group.add_argument(
            "--active",
            action="store_true",
            help="Afficher uniquement les adhérents avec une cotisation active.",
        )
        act_group.add_argument(
            "--unknown",
            action="store_true",
            help="Afficher uniquement les adhérents dont la cotisation "
            "n`est pas connue.",
        )
        act_group.add_argument(
            "--inactive",
            action="store_true",
            help="Afficher uniquement les adhérents dont la cotisation a été résiliée.",
        )

    def handle(self, *args, **options):
        adhesions = Adhesion.objects.all()
        if options["active"]:
            adhesions = adhesions.filter(active=True)
        elif options["unknown"]:
            adhesions = adhesions.filter(active=None)
        elif options["inactive"]:
            adhesions = adhesions.filter(active=False)
        if options["newer_than"]:
            adhesions = adhesions.filter(
                created__gt=now() - timedelta(days=options["newer_than"]),
            )
        adhesions = adhesions.select_related("user", "corporation")
        if options["emails"]:
            for adhesion in adhesions.order_by("pk"):
                email = adhesion.adherent.email
                if not email:
                    continue
                self.stdout.write(f"{adhesion.adherent} - ADT{adhesion.id} <{email}>")
        else:
            for adhesion in adhesions.order_by("pk"):
                if options["physique"] and adhesion.is_moral():
                    continue
                if options["morale"] and adhesion.is_physical():
                    continue
                line = "{} ({}): {}".format(
                    adhesion,
                    "PM" if adhesion.is_moral() else "PP",
                    adhesion.adherent,
                )
                if adhesion.adherent.email:
                    line += f", {adhesion.adherent.email}"
                line += f", {adhesion.created:%Y-%m-%d}"
                self.stdout.write(line)
