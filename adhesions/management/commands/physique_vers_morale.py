from django.core.management.base import BaseCommand, CommandError

from adhesions.models import Adhesion, Corporation


def boolean_input(question, default=None):
    result = input("%s " % question)
    if not result and default is not None:
        return default
    while len(result) < 1 or result[0].lower() not in "yn":
        result = input("Please answer yes or no: ")
    return result[0].lower() == "y"


class Command(BaseCommand):
    help = "Convertir une personne physique en personne morale"

    def add_arguments(self, parser):
        parser.add_argument("adt", type=int, help="Numéro d`adhérent")
        parser.add_argument("raison_sociale", help="Raison sociale")

    def handle(self, *args, **options):
        pk = options["adt"]
        try:
            adh = Adhesion.objects.get(pk=pk)
        except Adhesion.DoesNotExist:
            err = "L`adherent ADT%d n`existe pas." % pk
            raise CommandError(err) from err
        if adh.corporation:
            raise CommandError("L`adherent ADT%d est déjà une personne morale." % pk)
        user = adh.user
        if user.first_name != "FIXME":
            self.stdout.write(
                self.style.WARNING(
                    "L`adherent ADT%d s`est vu spécifier un prénom (%s), "
                    "risque de perte de données." % (pk, user.first_name),
                ),
            )
            if not boolean_input("Continuer ? [y/n]"):
                return
        if user.last_name != "%d" % pk:
            self.stdout.write(
                self.style.WARNING(
                    "L`adherent ADT%d s`est vu spécifier un nom (%s), "
                    "risque de perte de données." % (pk, user.last_name),
                ),
            )
            if not boolean_input("Continuer ? [y/n]"):
                return
        email = user.email
        tel = user.profile.phone_number
        address = user.profile.address
        if user.profile.ssh_keys:
            self.stdout.write(
                self.style.WARNING(
                    "L`adherent ADT%d s`est vu spécifier une clef SSH, "
                    "risque de perte de données." % pk,
                ),
            )
            if not boolean_input("Continuer ? [y/n]"):
                return
        notes = user.profile.notes
        corporation = Corporation(
            social_reason=options["raison_sociale"],
            email=email,
            phone_number=tel,
            address=address,
            notes=notes,
        )
        corporation.skip_adhesion_creation = True
        corporation.save()
        Adhesion.objects.filter(pk=pk).update(user=None, corporation=corporation)
        user.delete()
        self.stdout.write(
            self.style.SUCCESS(
                "Adhérent ADT%d convertie en personne morale avec succès." % pk,
            ),
        )
