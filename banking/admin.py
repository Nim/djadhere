import io

from django.contrib import admin
from django.core.exceptions import PermissionDenied
from django.db import models
from django.db.models import ExpressionWrapper, F, Sum
from django.db.models.functions import Coalesce
from django.forms import BaseInlineFormSet
from django.http import FileResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import path, re_path, reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from reportlab.pdfgen import canvas

from adhesions.admin import AdhesionAdmin
from services.admin import ServiceAdmin
from services.models import Service

from .facture import facture
from .models import (
    AccountBalance,
    Echeance,
    Expense,
    ExpenseCategory,
    Invoice,
    InvoicedProduct,
    PaymentUpdate,
    RecurringExpense,
    RecurringExpensePayment,
    RecurringPayment,
)
from .utils import notify_payment_update

# ## Inlines


class PaymentMethodFilter(admin.SimpleListFilter):
    title = "méthode de paiement"
    parameter_name = "method"

    def lookups(self, request, model_admin):
        return PaymentUpdate.PAYMENT_CHOICES

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(payment_method=self.value())
        return queryset.exclude(payment_method=PaymentUpdate.STOP)


class PendingPaymentFilter(admin.SimpleListFilter):
    title = "statut"
    parameter_name = "status"

    def lookups(self, request, model_admin):
        return (
            ("up-to-date", "À jour"),
            ("pending", "En attente"),
        )

    def queryset(self, request, queryset):
        if self.value() == "up-to-date":
            queryset = queryset.filter(last=True)
        if self.value() == "pending":
            queryset = queryset.filter(last=False)
        return queryset
        payments = RecurringPayment.objects.annotate(
            validated=models.Subquery(
                PaymentUpdate.objects.filter(payment=models.OuterRef("pk"))
                .filter(validated=True)
                .order_by("-start")
                .values("pk")[:1],
            ),
            pending=models.Subquery(
                PaymentUpdate.objects.filter(payment=models.OuterRef("pk"))
                .filter(validated=False)
                .order_by("-start")
                .values("pk")[:1],
            ),
        )
        up_to_date = payments.filter(pending__isnull=True)
        pending = payments.exclude(pending__isnull=True)
        if self.value() == "up-to-date":
            p = up_to_date
        elif self.value() == "pending":
            p = pending
        else:
            p = payments
        queryset = queryset.filter(pk__in=p.values_list("validated", flat=True))
        return queryset.annotate(
            last=models.Case(
                models.When(
                    pk__in=up_to_date.values_list("validated", flat=True),
                    then=True,
                ),
                default=False,
                output_field=models.BooleanField(),
            ),
        )


class PaymentTypeFilter(admin.SimpleListFilter):
    title = "type"
    parameter_name = "type"

    def lookups(self, request, model_admin):
        return (
            ("adhesion", "Adhésion"),
            ("service", "Service"),
        )

    def queryset(self, request, queryset):
        if self.value() == "adhesion":
            return queryset.filter(payment__adhesion__isnull=False)
        if self.value() == "service":
            return queryset.filter(payment__service__isnull=False)
        return None


# ## Inlines


class PendingPaymentUpdateFormSet(BaseInlineFormSet):
    def save_new(self, form, commit=True):
        obj = super().save_new(form, commit)
        if not obj.validated:
            notify_payment_update(self.request, obj)
        return obj

    def save_existing(self, form, instance, commit=True):
        old = PaymentUpdate.objects.get(pk=instance.pk)
        if not instance.validated:
            notify_payment_update(self.request, instance, old)
        return super().save_existing(form, instance, commit)


class PendingPaymentUpdateInline(admin.TabularInline):
    model = PaymentUpdate
    formset = PendingPaymentUpdateFormSet
    extra = 1
    max_num = 1
    verbose_name_plural = "Demande de saisie bancaire"

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        formset.request = request
        return formset

    def get_queryset(self, request):
        return super().get_queryset(request).filter(validated=False)


class ValidatedPaymentUpdateInline(admin.TabularInline):
    model = PaymentUpdate
    verbose_name_plural = "Historique"
    max_num = 0
    fields = (
        "amount",
        "period",
        "payment_method",
        "start",
    )
    readonly_fields = (
        "amount",
        "period",
        "payment_method",
        "start",
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        return super().get_queryset(request).filter(validated=True)


# ## Helpers


def prefix_search_field(prefix, field):
    if field[0] == "=":
        return "=" + prefix + "__" + field[1:]
    return prefix + "__" + field


# ## ModelAdmin


@admin.register(RecurringPayment)
class RecurringPaymentAdmin(admin.ModelAdmin):
    inlines = (
        PendingPaymentUpdateInline,
        ValidatedPaymentUpdateInline,
    )
    fields = (
        "payment_type",
        "payment_object_link",
        "debtor_link",
        "notes",
    )
    readonly_fields = (
        "payment_type",
        "payment_object_link",
        "debtor_link",
    )

    @admin.display(
        description="Objet",
    )
    def payment_object_link(self, obj):
        obj = obj.payment_object()
        return format_html('<a href="{}">{}</a>', obj.get_absolute_url(), obj)

    @admin.display(
        description="Débiteur",
    )
    def debtor_link(self, obj):
        url = reverse(
            viewname=f"admin:{obj._meta.app_label}_{obj._meta.model_name}_debtor",
            args=[obj.pk],
        )
        return format_html('<a href="{}">{}</a>', url, obj.debtor)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return obj

    def has_delete_permission(self, request, obj=None):
        return False

    def get_urls(self):
        info = self.model._meta.app_label, self.model._meta.model_name
        urls = [
            re_path(
                r"^(.*)/debtor/$",
                self.admin_site.admin_view(self.debtor_view),
                name="{}_{}_debtor".format(*info),
            ),
        ]
        return urls + super().get_urls()

    def debtor_view(self, request, payment_pk):
        payment = get_object_or_404(RecurringPayment, pk=payment_pk)
        adhesion = payment.debtor
        if adhesion.is_physical():
            profile = adhesion.user.profile
        else:
            profile = adhesion.corporation
        context = dict(
            self.admin_site.each_context(request),
            opts=self.model._meta,
            payment=payment,
            adhesion=adhesion,
            adherent=adhesion.adherent,
            profile=profile,
        )
        return TemplateResponse(request, "banking/admin/debtor.html", context)


@admin.register(PaymentUpdate)
class PaymentUpdateAdmin(admin.ModelAdmin):
    list_display = (
        "payment_type",
        "payment_object_link",
        "payment_link",
        "last",
    )
    list_select_related = (
        "payment",
        "payment__adhesion",
        "payment__service",
        "payment__service__service_type",
    )
    list_filter = (
        PaymentTypeFilter,
        PaymentMethodFilter,
        PendingPaymentFilter,
    )
    list_display_links = None
    search_fields = tuple(
        prefix_search_field("payment__adhesion", f) for f in AdhesionAdmin.search_fields
    ) + tuple(
        prefix_search_field("payment__service", f) for f in ServiceAdmin.search_fields
    )

    @admin.display(
        description="À jour",
        boolean=True,
    )
    def last(self, obj):
        return obj.last

    @admin.display(
        description="Type",
    )
    def payment_type(self, update):
        return update.payment.payment_type()

    @admin.display(
        description="Objet",
    )
    def payment_object_link(self, update):
        obj = update.payment.payment_object()
        return format_html('<a href="{}">{}</a>', obj.get_absolute_url(), obj)

    @admin.display(
        description="Paiement",
    )
    def payment_link(self, update):
        payment = update.payment
        return format_html('<a href="{}">{}</a>', payment.get_absolute_url(), update)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        payments = RecurringPayment.objects.annotate(
            validated=models.Subquery(
                PaymentUpdate.objects.filter(payment=models.OuterRef("pk"))
                .filter(validated=True)
                .order_by("-start")
                .values("pk")[:1],
            ),
            pending=models.Subquery(
                PaymentUpdate.objects.filter(payment=models.OuterRef("pk"))
                .filter(validated=False)
                .order_by("-start")
                .values("pk")[:1],
            ),
        )
        qs = qs.filter(pk__in=payments.values_list("validated", flat=True))
        return qs.annotate(
            last=models.Case(
                models.When(
                    pk__in=payments.filter(pending__isnull=True).values_list(
                        "validated",
                        flat=True,
                    ),
                    then=True,
                ),
                default=False,
                output_field=models.BooleanField(),
            ),
        )

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return not obj

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Echeance)
class EcheanceAdmin(admin.ModelAdmin):
    list_display = (
        "date",
        "adhesion",
        "amount",
    )
    list_filter = (
        "date",
        "status",
    )
    search_fields = ("=adhesion__pk",)


@admin.register(Expense)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = (
        "date",
        "amount",
        "recipient",
        "label",
        "category",
    )
    list_filter = (
        "date",
        "recipient",
        "category",
    )


class RecurringExpensePaymentInline(admin.TabularInline):
    model = RecurringExpensePayment


@admin.register(RecurringExpense)
class RecurringExpenseAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "period",
        "payment_method",
        "recipient",
        "label",
    )
    list_filter = (
        "recipient",
        "payment_method",
    )
    inlines = [RecurringExpensePaymentInline]


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_filter = ("name",)


class InvoicedProductAdmin(admin.TabularInline):
    model = InvoicedProduct
    verbose_name_plural = "Produit"

    def get_max_num(self, request, obj=None, **kwargs):
        if obj and obj.pdf:
            return 0
        return super().get_max_num(request, obj, **kwargs)

    def has_change_permission(self, request, obj=None):
        if obj and obj.pdf:
            return False
        return True

    def has_delete_permission(self, request, obj=None):
        if obj and obj.pdf:
            return False
        return True


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
        "reference",
        "date",
        "adhesion_link",
        "total",
        "invoiced",
        "sent",
        "paid",
    )
    raw_id_fields = ("buyer",)
    list_filter = ("date",)
    search_fields = (
        "=num",
        "=buyer__id",
    )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.annotate(
            total=Coalesce(
                Sum(
                    ExpressionWrapper(
                        F("items__quantity") * F("items__price"),
                        output_field=models.DecimalField(decimal_places=2),
                    ),
                ),
                0,
                output_field=models.DecimalField(),
            ),
        )

    @admin.display(
        description="Montant",
        ordering="total",
    )
    def total(self, invoice):
        return "%.2f €" % invoice.total

    @admin.display(
        description="Émise",
        boolean=True,
    )
    def invoiced(self, invoice):
        return bool(invoice.pdf)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return (
                "date",
                "reference",
                "adhesion_link",
                "total",
                "membership",
                "services",
            )
        return []

    def get_fieldsets(self, request, obj=None):
        fieldsets = []
        if obj:
            fieldsets += [
                (
                    None,
                    {
                        "fields": (
                            "date",
                            "reference",
                            "adhesion_link",
                            "total",
                            "sent",
                            "paid",
                            "notes",
                        ),
                    },
                ),
            ]
            if not obj.pdf:
                fieldsets += [
                    (
                        "Voir les cotisation et services en cours",
                        {
                            "fields": ("membership", "services"),
                            "classes": ("collapse",),
                        },
                    ),
                ]
        else:
            fieldsets += [
                (
                    None,
                    {
                        "fields": ("buyer",),
                    },
                ),
            ]
        return fieldsets

    def get_inlines(self, request, obj=None):
        if obj:
            return (InvoicedProductAdmin,)
        return ()

    @admin.display(
        description="Cotisation",
    )
    def membership(self, obj):
        return format_html(
            '<a href="{}">{}</a>',
            obj.buyer.membership.get_absolute_url(),
            obj.buyer.membership.get_current_payment_display(),
        )

    @admin.display(
        description="Services",
    )
    def services(self, obj):
        services = Service.objects.filter(adhesion=obj.buyer, active=True)
        services_display = []
        for service in services:
            description = format_html(
                '<a href="{}">{}</a>',
                service.get_absolute_url(),
                service,
            )
            contribution = format_html(
                '<a href="{}">{}</a>',
                service.contribution.get_absolute_url(),
                service.contribution.get_current_payment_display(),
            )
            services_display.append(f"{description}: {contribution}")
        return mark_safe("<br/>".join(services_display))

    @admin.display(
        description="Adhérent",
    )
    def adhesion_link(self, obj):
        url = reverse("admin:adhesions_adhesion_change", args=[obj.buyer.pk])
        return format_html('<a href="{}">{}</a>', url, str(obj.buyer))

    def has_delete_permission(self, request, obj=None):
        if not obj:
            return False
        if obj.pdf:
            return False
        if obj == Invoice.objects.order_by("-date", "-num").first():
            return True
        return False

    def get_urls(self):
        my_urls = [
            path(
                "<int:pk>/draft/",
                self.admin_site.admin_view(self.draft_invoice_pdf_view),
                name="banking_invoice_draft",
            ),
            path(
                "<int:pk>/generate/",
                self.admin_site.admin_view(self.generate_invoice_pdf_view),
                name="banking_invoice_generate",
            ),  # TODO: csrf protect
            path(
                "<int:pk>/pdf/",
                self.admin_site.admin_view(self.download_invoice_pdf_view),
                name="banking_invoice_pdf",
            ),  # TODO: cacheable=True
            path(
                "<int:pk>/copy/",
                self.admin_site.admin_view(self.copy_invoice_view),
                name="banking_invoice_copy",
            ),  # TODO: csrf protect
        ]
        return my_urls + super().get_urls()

    def _generate_invoice(self, invoice, draft=False):
        buffer = io.BytesIO()
        p = canvas.Canvas(buffer)
        produits = []
        for item in invoice.items.all():
            produits += [(item.description, item.notes, item.quantity, item.price)]
        adt = invoice.buyer.adherent
        facture(
            p,
            dt=invoice.date,
            ref=invoice.reference,
            produits=produits,
            adt=str(adt),
            mail=adt.email,
            tel=adt.phone_number,
            addr=adt.address,
            draft=draft,
        )
        buffer.seek(0)
        return buffer

    def draft_invoice_pdf_view(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        buffer = self._generate_invoice(invoice, draft=True)
        return FileResponse(
            buffer,
            as_attachment=True,
            filename=f"{invoice.reference}-draft.pdf",
        )

    def generate_invoice_pdf_view(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        if invoice.pdf:
            raise PermissionDenied
        buffer = self._generate_invoice(invoice)
        invoice.pdf = buffer.read()
        invoice.save()
        self.log_change(request, invoice, "Génération du PDF.")
        return redirect(invoice.get_absolute_url())

    def download_invoice_pdf_view(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        if invoice.pdf is None:
            raise Http404
        # ref https://docs.djangoproject.com/en/3.0/howto/outputting-pdf/
        buffer = io.BytesIO()
        buffer.write(invoice.pdf)
        buffer.seek(0)
        return FileResponse(
            buffer,
            as_attachment=True,
            filename=f"{invoice.reference}.pdf",
        )

    def copy_invoice_view(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        new_invoice = Invoice.objects.create(buyer=invoice.buyer)
        for item in invoice.items.all():
            item.pk = None
            item.invoice = new_invoice
            item.save()
        return redirect(new_invoice.get_absolute_url())


@admin.register(AccountBalance)
class AccountBalanceAdmin(admin.ModelAdmin):
    list_display = ("date", "amount")
    list_filter = ("date",)
