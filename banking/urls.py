from django.urls import path

from . import views

urlpatterns = [
    path("transparence/income/", views.income, name="income"),
    path("transparence/outcome/", views.outcome, name="outcome"),
    path("transparence/balance/", views.balance, name="balance"),
]
