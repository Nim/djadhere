from django.conf import settings
from django.urls import reverse

from djadhere.utils import send_notification


def notify_payment_update(request, update, old_update=None):
    benevole = f"{request.user.username} <{request.user.email}>"
    message = "Bénévole : " + benevole
    message += "\n\nPaiement :\n\n"

    if old_update:
        message += "\t- %s\n" % old_update
        message += "\t+ %s\n" % update
        subject = "Mise à jour d`une demande de saisie bancaire"
    else:
        message += "\t%s\n" % update
        subject = "Nouvelle demande de saisie bancaire"
    subject += " %s %d" % (
        update.payment.payment_type(),
        update.payment.payment_object().pk,
    )

    base_url = "https" if request.is_secure() else "http"
    base_url += "://" + request.get_host()
    message += (
        "\nVoir : "
        + base_url
        + reverse("admin:banking_recurringpayment_change", args=(update.payment.pk,))
    )
    message += (
        "\nDébiteur : "
        + base_url
        + reverse("admin:banking_recurringpayment_debtor", args=(update.payment.pk,))
    )

    send_notification(subject, message, settings.PAYMENTS_EMAILS, cc=[benevole])
