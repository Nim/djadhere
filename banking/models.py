import datetime

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse
from django.utils import timezone


class CurrentPaymentManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        one_year_ago = timezone.now() - datetime.timedelta(days=365)
        return qs.annotate(
            payment_method=models.Subquery(
                PaymentUpdate.objects.filter(
                    payment=models.OuterRef("pk"),
                    validated=True,
                ).values("payment_method")[:1],
            ),
            payment_start=models.Subquery(
                PaymentUpdate.objects.filter(
                    payment=models.OuterRef("pk"),
                    validated=True,
                ).values("start")[:1],
            ),
            long_stopped=models.Case(
                models.When(
                    payment_method=PaymentUpdate.STOP,
                    payment_start__lte=one_year_ago,
                    then=True,
                ),
                default=False,
                output_field=models.BooleanField(),
            ),
            active=models.Case(
                models.When(payment_method__isnull=True, then=None),
                models.When(payment_method=PaymentUpdate.STOP, then=False),
                default=True,
                output_field=models.BooleanField(null=True),
            ),
        )


class RecurringPayment(models.Model):
    notes = models.TextField(blank=True, default="")

    objects = CurrentPaymentManager()

    @property
    def debtor(self):
        if hasattr(self, "adhesion"):
            return self.adhesion
        if hasattr(self, "service"):
            return self.service.adhesion
        return None

    def payment_type(self):
        if hasattr(self, "adhesion"):
            return "Adhésion"
        if hasattr(self, "service"):
            return "Service"
        return None

    payment_type.short_description = "Type"

    def payment_object(self):
        return self.adhesion if hasattr(self, "adhesion") else self.service

    def get_absolute_url(self):
        return reverse(
            f"admin:{self._meta.app_label}_{self._meta.model_name}_change",
            args=(self.pk,),
        )

    def get_current_payment(self):
        return self.updates.filter(validated=True).first()

    def get_current_payment_display(self):
        current = self.get_current_payment()
        if current:
            return str(current)
        return "non renseignée"

    class Meta:
        verbose_name = "paiement récurrent"
        verbose_name_plural = "paiements récurrents"

    def __str__(self):
        if hasattr(self, "adhesion"):
            return "Cotisation %s" % self.adhesion
        return "Contribution %s" % self.service


class PaymentUpdate(models.Model):
    STOP = 0
    FREE = 1
    DEBIT = 2
    TRANSFER = 3
    CASH = 4
    INVOICE = 5
    PAYMENT_CHOICES = (
        (DEBIT, "Prélèvement"),
        (TRANSFER, "Virement"),
        (CASH, "Liquide"),
        (INVOICE, "Facture"),
        (FREE, "Gratuit"),
        (STOP, "Arrêt"),
    )
    created = models.DateTimeField(auto_now_add=True)
    payment = models.ForeignKey(
        RecurringPayment,
        related_name="updates",
        on_delete=models.CASCADE,
    )
    amount = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Montant")
    period = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(12)],
        verbose_name="Période (mois)",
    )
    payment_method = models.IntegerField(
        choices=PAYMENT_CHOICES,
        default=DEBIT,
        verbose_name="Méthode de paiement",
    )
    start = models.DateTimeField(verbose_name="Date")
    month_day = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(25)],
        verbose_name="Jour souhaité de prélèvement",
        null=True,
        blank=True,
    )
    validated = models.BooleanField(
        default=False,
        verbose_name="Saisie bancaire effectuée",
    )

    class Meta:
        verbose_name = "paiement"
        ordering = ("-start",)

    def clean(self):
        super().clean()
        errors = {}
        if self.payment_method == PaymentUpdate.STOP and self.amount:
            errors.update(
                {"amount": "Pour l`arrêt d`un paiement, le montant doit être nul."},
            )
        if errors:
            raise ValidationError(errors)

    def period_verbose(self):
        if self.period == 0:
            return "non récurrent"
        if self.period == 1:
            return "mensuel"
        if self.period == 3:
            return "trimestriel"
        if self.period == 6:
            return "biannuel"
        if self.period == 12:
            return "annuel"
        return "%d mois" % self.period

    def __str__(self):
        if self.payment_method == self.STOP:
            return "paiement arrêté"
        if self.payment_method == self.FREE:
            return "libre"
        s = str(self.amount) + "€"
        if self.period:
            if self.period == 1:
                s += "/mois"
            elif self.period == 12:
                s += "/an"
            else:
                s += "/%d mois" % self.period
        if self.payment_method == self.DEBIT:
            s += " (prélèvement)"
        elif self.payment_method == self.TRANSFER:
            s += " (virement)"
        elif self.payment_method == self.CASH:
            s += " (liquide)"
        elif self.payment_method == self.INVOICE:
            s += " (sur facture)"
        return s


class Echeance(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateField(verbose_name="Date")
    reference = models.IntegerField(unique=True, verbose_name="référence")
    adhesion = models.ForeignKey(
        "adhesions.Adhesion",
        related_name="echeances",
        on_delete=models.CASCADE,
        verbose_name="Adhérent",
    )
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name="Montant",
    )
    status = models.CharField(max_length=64, verbose_name="Status")

    class Meta:
        verbose_name = "échéance"
        ordering = ("-date",)

    def __str__(self):
        return f"{self.amount} € ({self.adhesion})"


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True, default="")

    class Meta:
        verbose_name = "catégorie de dépense"
        verbose_name_plural = "catégories de dépense"

    def __str__(self):
        return self.name


class Expense(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateField(verbose_name="Date")
    amount = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Montant")
    recipient = models.CharField(max_length=256, verbose_name="Fournisseur")
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="expenses",
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Catégorie",
    )
    label = models.CharField(max_length=256, verbose_name="Intitulé")
    details = models.TextField(
        blank=True,
        default="",
        verbose_name="Détails",
        help_text="Informations publics.",
    )
    notes = models.TextField(
        blank=True,
        default="",
        verbose_name="Notes",
        help_text="Informations internes (référence de facture, …).",
    )

    class Meta:
        verbose_name = "dépense"
        verbose_name_plural = "dépenses"
        ordering = ("-date",)

    def __str__(self):
        return f"{self.recipient} - {self.label}"


class RecurringExpense(models.Model):
    UNKNOWN = 0
    DEBIT = 1
    TRANSFER = 2
    CASH = 3
    PREPAYMENT = 4
    PAYMENT_CHOICES = (
        (DEBIT, "Inconnu"),
        (DEBIT, "Prélèvement"),
        (TRANSFER, "Virement"),
        (CASH, "Liquide"),
        (PREPAYMENT, "Pré-paiement"),
    )
    created = models.DateTimeField(auto_now_add=True)
    period = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(24)],
        verbose_name="Période (mois)",
    )
    amount = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Montant")
    payment_method = models.IntegerField(
        choices=PAYMENT_CHOICES,
        default=UNKNOWN,
        verbose_name="Méthode de paiement",
    )
    recipient = models.CharField(max_length=256, verbose_name="Fournisseur")
    label = models.CharField(max_length=256, verbose_name="Intitulé")
    notes = models.TextField(blank=True, default="")

    class Meta:
        verbose_name = "dépense récurrente"
        verbose_name_plural = "dépenses récurrentes"
        ordering = (
            "recipient",
            "label",
        )

    def __str__(self):
        return f"{self.recipient} - {self.label}"


class RecurringExpensePayment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateField(verbose_name="Date")
    amount = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Montant")
    expense = models.ForeignKey(
        "banking.RecurringExpense",
        related_name="payments",
        null=True,
        default=None,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name="Dépense récurente",
    )
    label = models.CharField(max_length=256, verbose_name="Intitulé")

    class Meta:
        verbose_name = "paiement manuel"
        verbose_name_plural = "paiements manuels"
        ordering = ("-date",)

    def __str__(self):
        return f"{self.expense} - {self.label}"


def get_default_account():
    from adhesions.models import Adhesion

    return Adhesion.objects.get(id=100)


class Invoice(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateField(verbose_name="Date", default=datetime.date.today)
    num = models.IntegerField()
    buyer = models.ForeignKey(
        "adhesions.Adhesion",
        default=get_default_account,
        related_name="invoices",
        on_delete=models.SET_DEFAULT,
        verbose_name="Adhérent",
    )
    notes = models.TextField(blank=True, default="")
    pdf = models.BinaryField(null=True)
    sent = models.BooleanField(verbose_name="Envoyée", null=True)
    paid = models.BooleanField(verbose_name="Payée", null=True)

    @property
    def reference(self):
        return "F{:04d}{:02d}{:02d}-{:04d}-ADT{}".format(
            self.date.year,
            self.date.month,
            self.date.day,
            self.num,
            self.buyer.pk,
        )

    def get_absolute_url(self):
        return reverse(
            f"admin:{self._meta.app_label}_{self._meta.model_name}_change",
            args=(self.pk,),
        )

    def save(self, *args, **kwargs):
        if not self.pk:
            self.date = datetime.date.today()
            last_invoice = (
                Invoice.objects.filter(date__year=self.date.year).order_by("num").last()
            )
            self.num = last_invoice.num + 1 if last_invoice is not None else 1
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "facture"
        verbose_name_plural = "factures"
        ordering = (
            "-date",
            "-num",
        )

    def __str__(self):
        return self.reference


class InvoicedProduct(models.Model):
    invoice = models.ForeignKey(Invoice, related_name="items", on_delete=models.CASCADE)
    description = models.CharField(max_length=64)
    notes = models.CharField(max_length=64, blank=True, default="")
    quantity = models.IntegerField(verbose_name="Quantité")
    price = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        verbose_name="Montant unitaire",
    )

    def __str__(self):
        return f"{self.invoice} - {self.description}"


class AccountBalance(models.Model):
    date = models.DateField(verbose_name="Date", default=datetime.date.today)
    amount = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Solde")

    class Meta:
        verbose_name = "solde"
        verbose_name_plural = "soldes"
        ordering = ("-date",)

    def __str__(self):
        return f"{self.amount:.2f} €"
