import csv
from datetime import date

from django.contrib.auth.decorators import login_required
from django.db.models import DecimalField, ExpressionWrapper, F, Sum
from django.db.models.functions import ExtractYear
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.cache import cache_page

from .models import AccountBalance, Expense, ExpenseCategory, RecurringExpense
from .utils import Transparency

SERVICES_HEAD = (
    ("total", "nombre total de services de ce type"),
    ("infra", "nombre de services au nom de l`association"),
    ("adhérent·e·s", "nombre de services au nom d`un·e adhérent·e"),
    ("gratuits", "nombre de services fournis gracieusement"),
    (
        "manquants",
        "nombre de services dont les informations de paiement sont manquantes "
        "(généralement non payées mais pas nécessairement volontairement)",
    ),
    ("payés", "nombre de services effectivement payés"),
    ("euros", "recette total pour ce type de service"),
    (
        "moyenne",
        "contribution moyenne "
        "(calculé sur les services effectivement payés uniquement)",
    ),
    ("pourcent", "proportion des recettes de l`association dues à ce type de service"),
)

ADHESIONS_HEAD = (
    ("total", "nombre total d`adhésions"),
    ("gratuites", "nombre d`adhésions gracieuses"),
    (
        "manquantes",
        "nombre d`adhésions dont les informations de paiement sont manquantes "
        "(généralement non payées mais pas nécessairement volontairement)",
    ),
    ("payées", "nombre d`adhésions payées"),
    ("euros", "recette moyenne des adhésions sur un mois"),
    (
        "moyenne",
        "montant d`adhésion moyen (calculé uniquement sur les adhésions payées)",
    ),
)


@cache_page(60 * 60 * 8)  # 8h
def income(request):
    fmt = request.GET.get("fmt", "html")
    t = Transparency()
    if fmt == "csv":
        response = HttpResponse(content_type="text/csv")
        today = date.today().strftime("%Y_%m_%d")
        response["Content-Disposition"] = (
            'attachment; filename="tetaneutral_transparence_%s.csv"' % today
        )
        writer = csv.writer(response)
        writer.writerow(title for title, _ in SERVICES_HEAD)
        for service in t.services():
            writer.writerow(
                [
                    service.name,
                    service.total,
                    service.infra,
                    service.adh,
                    service.free,
                    service.miss,
                    service.income,
                    service.euros,
                    service.mean,
                    service.percent,
                ],
            )
        writer.writerow(title for title, _ in ADHESIONS_HEAD)
        adhesions = t.adhesions()
        writer.writerow(
            [
                adhesions.total,
                adhesions.free,
                adhesions.miss,
                adhesions.pay,
                adhesions.income,
                adhesions.mean,
            ],
        )
        return response
    return render(
        request,
        "banking/income.html",
        {
            "services": t.services(),
            "adhesions": t.adhesions(),
            "services_head": SERVICES_HEAD,
            "adhesions_head": ADHESIONS_HEAD,
        },
    )


@login_required
def outcome(request):
    recurring_expenses = RecurringExpense.objects.annotate(
        monthly_amount=ExpressionWrapper(
            F("amount") / F("period"),
            output_field=DecimalField(),
        ),
    )
    non_recurring_expenses = Expense.objects.all()
    yearly_non_recurring_expenses = (
        non_recurring_expenses.order_by("date")
        .annotate(year=ExtractYear("date"))
        .values("year")
        .annotate(yearly_amount=Sum("amount"))
        .order_by("year")
    )
    total_recurring_expenses = recurring_expenses.aggregate(
        total_monthly_amount=Sum("monthly_amount"),
    )
    _by_categories_yearly_non_recurring_expenses = (
        non_recurring_expenses.annotate(year=ExtractYear("date"))
        .order_by("category__name", "year")
        .values("category__name", "year")
        .annotate(yearly_amount=Sum("amount"))
    )
    categories = ExpenseCategory.objects.all()
    by_categories_yearly_non_recurring_expenses = {}
    for yearly_expenses in yearly_non_recurring_expenses:
        for category in categories:
            if category.name not in by_categories_yearly_non_recurring_expenses:
                by_categories_yearly_non_recurring_expenses[category.name] = {}
            by_categories_yearly_non_recurring_expenses[category.name][
                yearly_expenses["year"]
            ] = 0
    for entry in _by_categories_yearly_non_recurring_expenses:
        by_categories_yearly_non_recurring_expenses[entry["category__name"]][
            entry["year"]
        ] = entry["yearly_amount"]
    bcynre = "by_categories_yearly_non_recurring_expenses"
    return render(
        request,
        "banking/outcome.html",
        {
            "non_recurring_expenses": non_recurring_expenses,
            "yearly_non_recurring_expenses": yearly_non_recurring_expenses,
            bcynre: by_categories_yearly_non_recurring_expenses,
            "recurring_expenses": recurring_expenses,
            "monthly_recurring_expenses": total_recurring_expenses[
                "total_monthly_amount"
            ],
            "categories": categories,
        },
    )


@login_required
def balance(request):
    return render(
        request,
        "banking/balance.html",
        {"balances": AccountBalance.objects.all()},
    )
