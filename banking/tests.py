from datetime import datetime

from django.test import TestCase
from django.urls import reverse

from accounts.models import User
from adhesions.models import Adhesion, Corporation

from .models import PaymentUpdate


class BankingTest(TestCase):
    def setUp(self):
        User.objects.create_user(
            "admin",
            email="admin@example.net",
            password="admin",
            is_staff=True,
            is_superuser=True,
        )
        user = User.objects.create_user(
            "user",
            first_name="first",
            last_name="last",
            email="user@example.net",
            password="user",
        )
        corp = Corporation.objects.create(social_reason="GoodCorp")
        corp.members.add(user)

    def test_admin(self):
        self.client.login(username="admin", password="admin")
        for model in [
            "expensecategory",
            "expense",
            "recurringexpense",
            "invoice",
            "paymentupdate",
            "recurringpayment",
            "accountbalance",
            "echeance",
        ]:
            response = self.client.get(reverse(f"admin:banking_{model}_changelist"))
            self.assertEqual(response.status_code, 200)

    def test_views(self):
        # ADT100 is expected…
        for i in range(101 - Adhesion.objects.count()):
            Corporation.objects.create(social_reason=f"workaround {i}")

        # we need at least one membership to avoid a division by 0
        user = User.objects.get(username="user")
        PaymentUpdate.objects.create(
            payment=user.adhesion.membership,
            amount=20,
            period=12,
            payment_method=4,
            start=datetime.now(),
            validated=True,
        )
        response = self.client.get(reverse("income"))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("income") + "?fmt=csv")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("outcome"))
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse("balance"))
        self.assertEqual(response.status_code, 302)
        self.client.login(username="user", password="user")
        response = self.client.get(reverse("outcome"))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("balance"))
        self.assertEqual(response.status_code, 200)
