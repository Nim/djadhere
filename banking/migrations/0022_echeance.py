# Generated by Django 3.0.5 on 2020-10-01 23:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("adhesions", "0020_auto_20190227_2058"),
        ("banking", "0021_auto_20201001_1454"),
    ]

    operations = [
        migrations.CreateModel(
            name="Echeance",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("date", models.DateField(verbose_name="Date")),
                (
                    "reference",
                    models.IntegerField(unique=True, verbose_name="référence"),
                ),
                (
                    "amount",
                    models.DecimalField(
                        decimal_places=2, max_digits=10, verbose_name="Montant"
                    ),
                ),
                ("status", models.CharField(max_length=64, verbose_name="Status")),
                (
                    "adhesion",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="echeances",
                        to="adhesions.Adhesion",
                        verbose_name="Adhérent",
                    ),
                ),
            ],
            options={
                "verbose_name": "échéance",
                "ordering": ("-date",),
            },
        ),
    ]
