# Generated by Django 3.0.5 on 2021-02-05 20:12

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("banking", "0023_auto_20210204_1343"),
    ]

    operations = [
        migrations.CreateModel(
            name="AccountBalance",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "date",
                    models.DateField(default=datetime.date.today, verbose_name="Date"),
                ),
                (
                    "amount",
                    models.DecimalField(
                        decimal_places=2, max_digits=9, verbose_name="Solde"
                    ),
                ),
            ],
            options={
                "verbose_name": "solde",
                "verbose_name_plural": "soldes",
                "ordering": ("-date",),
            },
        ),
        migrations.AlterField(
            model_name="expense",
            name="category",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="expenses",
                to="banking.ExpenseCategory",
                verbose_name="Catégorie",
            ),
        ),
    ]
