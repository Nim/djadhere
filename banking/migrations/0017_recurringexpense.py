# Generated by Django 3.0.5 on 2020-09-29 20:20

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("banking", "0016_auto_20200617_0015"),
    ]

    operations = [
        migrations.CreateModel(
            name="RecurringExpense",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                (
                    "period",
                    models.PositiveIntegerField(
                        validators=[
                            django.core.validators.MinValueValidator(1),
                            django.core.validators.MaxValueValidator(24),
                        ],
                        verbose_name="Période (mois)",
                    ),
                ),
                (
                    "amount",
                    models.DecimalField(
                        decimal_places=2, max_digits=9, verbose_name="Montant"
                    ),
                ),
                (
                    "recipient",
                    models.CharField(max_length=256, verbose_name="Fournisseur"),
                ),
                ("label", models.CharField(max_length=256, verbose_name="Intitulé")),
                ("notes", models.TextField(blank=True, default="")),
            ],
            options={
                "verbose_name": "dépense récurrente",
                "verbose_name_plural": "dépenses récurrentes",
                "ordering": ("-created",),
            },
        ),
    ]
