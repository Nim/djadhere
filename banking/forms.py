from django.core.exceptions import ValidationError
from django.forms import ModelForm

from .models import Payment


class PaymentForm(ModelForm):
    class Meta:
        model = Payment
        fields = ["amount", "period", "payment_method", "start"]

    def clean_start(self):
        start = self.cleaned_data["start"]
        if self.instance.pk:
            if start < self.instance.start:
                msg = "valeur invalide"
                raise ValidationError(msg, code="invalid")
        return start
