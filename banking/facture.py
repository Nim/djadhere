#!/usr/bin/env python
# ruff: noqa: E501

import locale
from datetime import date

from reportlab.lib import colors
from reportlab.pdfgen import canvas
from reportlab.platypus import Table, TableStyle

TRESORIER = """c/o Sylvain REVAULT
7 Impasse René Char
Logement 39
31600 Lherm
Contact: adhesion@tetaneutral.net"""

ASSOCIATION = """Tetaneutral.net est une association à but non lucratif non soumise aux impôts commerciaux.
Règlement par virement avec comme motif de virement le numéro de facture sur le compte suivant :
RIB: 42559 10000 08012847472 70
IBAN: FR76 4255 9100 0008 0128 4747 270
BIC: CCOPFRPPXXX
Ou par chèque à l`ordre de « Association tetaneutral.net » avec la mention du numéro de facture jointe
sur papier libre."""


def eur(x: float) -> str:
    return f"{x:.2f} €"


def facture(c, dt, ref, produits, adt, addr, tel, mail, draft=False):
    locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")  # for month representation
    title = f"Facture n°{ref}"

    c.setAuthor("Tetaneutral.net")
    c.setTitle(title)

    textobject = c.beginText(50, 780)
    textobject.setFont("Times-Roman", 25)
    textobject.textLine("Association tetaneutral.net")
    textobject.setFont("Times-Roman", 12)
    textobject.textLines(TRESORIER)
    c.drawText(textobject)

    c.drawString(450, 670, f"Date: {dt:%_d %B %Y}")

    c.setFont("Times-Roman", 22)
    c.drawString(100, 630, title)

    textobject = c.beginText(50, 600)
    textobject.setFont("Times-Roman", 12)
    textobject.textLine("Livraison et Facturation:")
    textobject.textLine(adt)
    textobject.textLines(addr)
    if tel:
        textobject.textLine(f"tel: {tel}")
    if mail:
        textobject.textLine(f"courriel: {mail}")
    c.drawText(textobject)

    table = [["Produit", "Quantité", "Prix unitaire", "Total"]]
    total = 0
    for desc, comment, nb, prix in produits:
        if comment:
            desc += f"\n{comment}"
        table.append([desc, str(nb), eur(prix), eur(prix * nb)])
        total += prix * nb
    table.append(["", "", "Total", eur(total)])

    flow = Table(table, (260, 80, 80, 80))
    flow.setStyle(
        TableStyle(
            [
                ("BACKGROUND", (0, 0), (-1, 0), colors.grey),
                ("FONT", (0, 0), (-1, 0), "Times-Bold"),
                ("ALIGN", (0, 0), (-1, 0), "CENTER"),
                ("ALIGN", (1, 1), (-1, -1), "RIGHT"),
                ("BOX", (0, 0), (-1, -2), 0.5, colors.black),
                ("BOX", (0, -2), (-1, -1), 0.5, colors.black),
                ("INNERGRID", (0, 0), (-1, -2), 0.5, colors.black),
                ("INNERGRID", (-2, -2), (-1, -1), 0.5, colors.black),
            ],
        ),
    )
    _, table_height = flow.wrapOn(c, 600, (len(table) + 2) * 20)
    flow.drawOn(c, 50, 475 - table_height)

    textobject = c.beginText(50, 140)
    textobject.textLines(ASSOCIATION)
    c.drawText(textobject)

    if draft:
        textobject = c.beginText(300, 100)
        textobject.setFillColor(colors.Color(1, 0, 0, alpha=0.4))
        textobject.setFont("Times-Roman", 120)
        textobject.textLines("DRAFT")
        c.saveState()
        c.rotate(45)
        c.drawText(textobject)
        c.restoreState()

    c.showPage()
    c.save()


if __name__ == "__main__":
    # mock data
    data = {
        "dt": date(2020, 6, 3),
        "ref": "F20200603-0022-ADT268 (test nim)",
        "produits": [("Adhésion 2020", "", 1, 20), ("Hosting", "VM toto.ttnn", 12, 28)]
        * 6,
        "adt": "Mairie de Monès",
        "addr": "Couyre\n31370 MONES",
        "tel": "N/A",
        "mail": "mairie.mones@orange.fr",
    }

    c = canvas.Canvas("hello.pdf")
    facture(c, **data, draft=True)
