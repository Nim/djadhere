import re
from argparse import FileType
from collections import namedtuple
from csv import reader as csv_reader
from datetime import datetime
from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError

from adhesions.models import Adhesion
from banking.models import Echeance as EcheanceModel


class Command(BaseCommand):
    help = "Importer les échéances des prélèvements."

    def add_arguments(self, parser):
        parser.add_argument("echeances", type=FileType("r", encoding="utf-8-sig"))

    def handle(self, *args, **options):
        adt_regex = re.compile('="ADT(?P<pk>[0-9]+)[A-Za-z]?"')
        ref_regex = re.compile('="(?P<ref>[0-9]+)"')
        reader = csv_reader(options["echeances"], delimiter=";")
        headers = next(reader)
        mapping = {
            "rum": "RUM\xa0échéancier",
            "adt": "Référence débiteur",
            "reference": "Référence de bout en bout",
            "date": "Date d`échéance",
            "status": "Statut échéance",
            "amount": "Montant échéancier",
        }
        mapping = {k: headers.index(v) for k, v in mapping.items()}
        Echeance = namedtuple("Échéance", mapping.keys())
        added, already_existing = 0, 0
        for line in reader:
            echeance = Echeance(**{k: line[v] for k, v in mapping.items()})
            g = adt_regex.match(echeance.adt)
            if not g:
                msg = f"Unable to parse ADT '{echeance.adt}'."
                raise CommandError(msg)
            try:
                adt = Adhesion.objects.get(pk=g.group("pk"))
            except Adhesion.DoesNotExist as err:
                msg = "ADT{} not found.".format(g.group("pk"))
                raise CommandError(msg) from err
            date = datetime.strptime(echeance.date, "%d/%m/%y").date()
            amount = Decimal(echeance.amount.replace(",", "."))
            if echeance.status in [
                "En attente d'exécution",
                "Bloquée passée",
                "Supprimée",
            ]:
                continue
            if echeance.status not in ["Bloquée", "Impayée", "Exécutée"]:
                msg = f"Unknown status '{echeance.status}'."
                raise CommandError(msg)
            g = ref_regex.match(echeance.reference)
            if not g:
                msg = f"Unable to parse reference '{echeance.reference}'."
                raise CommandError(msg)
            reference = int(g.group("ref"))
            db_echeance, created = EcheanceModel.objects.get_or_create(
                reference=reference,
                defaults={
                    "date": date,
                    "amount": amount,
                    "adhesion": adt,
                    "status": echeance.status,
                },
            )
            if created:
                added += 1
            else:
                already_existing += 1
                assert db_echeance.date == date
                assert db_echeance.amount == amount
                assert db_echeance.adhesion == adt
                assert db_echeance.status == echeance.status
        print(f"Added: {added}, already existing: {already_existing}")
