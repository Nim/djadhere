from distutils.util import strtobool

from django.core.management.base import BaseCommand, CommandError

from banking.models import Expense, RecurringExpense, RecurringExpensePayment


class Command(BaseCommand):
    help = "Définir une dépense comme paiement d`une dépense récurrente."

    def add_arguments(self, parser):
        parser.add_argument("expense", type=int)
        parser.add_argument("recurring_expense", type=int)

    def handle(self, *args, **options):
        try:
            expense = Expense.objects.get(pk=options["expense"])
        except Expense.DoesNotExist as err:
            msg = "No expense with id '{}'.".format(options["expense"])
            raise CommandError(msg) from err
        self.stdout.write(f"Expense: {expense} ({expense.amount} €).")
        if expense.notes:
            self.stdout.write(f"Notes: {expense.notes}")
            msg = (
                "Expense note will be lost; please save these information elsewhere "
                "and empty the field."
            )
            raise CommandError(msg)
        try:
            recurring_expense = RecurringExpense.objects.get(
                pk=options["recurring_expense"],
            )
        except RecurringExpense.DoesNotExist as err:
            msg = "No recurring expense with id '{}'.".format(
                options["recurring_expense"],
            )
            raise CommandError(msg) from err
        self.stdout.write(f"Recurring expense: {recurring_expense}.")
        reply = None
        while reply is None:
            reply = str(input("Associate? (y/n) ")).lower().strip()
            try:
                reply = strtobool(reply)
            except ValueError:
                reply = None
        if not reply:
            return
        RecurringExpensePayment.objects.create(
            date=expense.date,
            amount=expense.amount,
            expense=recurring_expense,
            label=expense.label,
        )
        expense.delete()
